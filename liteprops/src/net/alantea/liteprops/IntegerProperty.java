package net.alantea.liteprops;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class IntegerProperty.
 */
public class IntegerProperty extends Property<Integer>
{
   /** The _logger. */
   private static Logger logger = LoggerFactory.getLogger(IntegerProperty.class);
   
   /**
    * Instantiates a new integer property.
    */
   public IntegerProperty()
   {
      super(0, true);
   }
   
   /**
    * Instantiates a new integer property.
    *
    * @param flag the flag
    */
   protected IntegerProperty(boolean flag)
   {
      super(0, flag);
   }

   /**
    * Instantiates a new integer property.
    *
    * @param value the value
    */
   public IntegerProperty(int value)
   {
      super(value, true);
   }
   
   /**
    * Instantiates a new integer property.
    *
    * @param value the value
    * @param flag the flag
    */
   protected IntegerProperty(int value, boolean flag)
   {
      super(value, flag);
   }

   /**
    * Add.
    *
    * @param property the property
    * @return the read only property
    */
   public IntegerProperty add(Property<?> property)
   {
      int bret = getIntFrom(this) + getIntFrom(property);
      IntegerProperty ret = new IntegerProperty(false);
      addListener((o, n) -> ret.setValue(getIntFrom(this) +  getIntFrom(property)));
      property.addListener((o, n) -> ret.setValue(getIntFrom(this) +  getIntFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * minus.
    *
    * @param property the property
    * @return the read only property
    */
   public IntegerProperty minus(Property<?> property)
   {
      int bret = getIntFrom(this) - getIntFrom(property);
      IntegerProperty ret = new IntegerProperty(false);
      addListener((o, n) -> ret.setValue(getIntFrom(this) -  getIntFrom(property)));
      property.addListener((o, n) -> ret.setValue(getIntFrom(this) -  getIntFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Divide.
    *
    * @param property the property
    * @return the read only property
    */
   public IntegerProperty divide(Property<?> property)
   {
      int bret = getIntFrom(this) / getIntFrom(property);
      IntegerProperty ret = new IntegerProperty(false);
      addListener((o, n) -> ret.setValue(getIntFrom(this) /  getIntFrom(property)));
      property.addListener((o, n) -> ret.setValue(getIntFrom(this) /  getIntFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Multiply.
    *
    * @param property the property
    * @return the read only property
    */
   public IntegerProperty multiply(Property<?> property)
   {
      int bret = getIntFrom(this) * getIntFrom(property);
      IntegerProperty ret = new IntegerProperty(false);
      addListener((o, n) -> ret.setValue(getIntFrom(this) *  getIntFrom(property)));
      property.addListener((o, n) -> ret.setValue(getIntFrom(this) *  getIntFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Minimum.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static IntegerProperty minimum(Property<?> property1, Property<?> property2)
   {
      IntegerProperty ret = new IntegerProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(Math.min(getIntFrom(property1), getIntFrom(property2)));
      }
      property1.addListener((o, n) -> ret.setValue(Math.min(getIntFrom(property1), getIntFrom(property2))));
      property2.addListener((o, n) -> ret.setValue(Math.min(getIntFrom(property1), getIntFrom(property2))));
      return ret;
   }
   
   /**
    * Maximum.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static IntegerProperty maximum(Property<?> property1, Property<?> property2)
   {
      IntegerProperty ret = new IntegerProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(Math.max(getIntFrom(property1), getIntFrom(property2)));
      }
      property1.addListener((o, n) -> ret.setValue(Math.max(getIntFrom(property1), getIntFrom(property2))));
      property2.addListener((o, n) -> ret.setValue(Math.max(getIntFrom(property1), getIntFrom(property2))));
      return ret;
   }
   
   /**
    * Gets the integer from any kind of property.
    *
    * @param property the property
    * @return the integer
    */
   public static int getIntFrom(Property<?> property)
   {
      Object content = property.get();
      if ((content instanceof Integer) || (content instanceof Long))
      {
         return (int)content;
      }
      else if (content instanceof Boolean)
      {
         return ((Boolean)content) ? 1 : 0;
      }
      else if (content instanceof Integer)
      {
         return ((Integer)content).intValue();
      }
      else if (content instanceof Long)
      {
         return ((Long)content).intValue();
      }
      else if (content instanceof Double)
      {
         return ((Double)content).intValue();
      }
      else if (content instanceof Float)
      {
         return ((Float)content).intValue();
      }
      else if (content instanceof String)
      {
         try
         {
            return Integer.parseInt((String)content);
         }
         catch (NumberFormatException e)
         {
            logger.error("Error converting to integer : " + content, e);
            return 0;
         }
      }
      return 0;
   }
}
