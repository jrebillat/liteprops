package net.alantea.liteprops;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * The Class MapProperty.
 *
 * @param <P> the generic type
 * @param <Q> the generic type
 */
public class MapProperty<P, Q> extends Property<Map<P, Q>> implements Map<P, Q>
{
   
   /** The size property. */
   private IntegerProperty sizeProperty = new IntegerProperty();
   
   /** The avoid map loop. */
   private boolean avoidMapLoop;
   
   /** The map. */
   private Map<P, Q> map;
   
   /**
    * Instantiates a new map property.
    */
   public MapProperty()
   {
      super(new HashMap<>());
      map = (Map<P, Q>) super.get();
      sizeProperty.set(0);
   }

   /**
    * Instantiates a new map property.
    *
    * @param value the value
    */
   public MapProperty(Map<P, Q> value)
   {
      this();
      map = value;
      setValue(value);
   }
   
   /**
    * Gets the map.
    *
    * @return the map
    */
   @Override
   public Map<P, Q> get()
   {
      return this;
   }
   
   /**
    * Sets thenew map as value.
    *
    * @param newValue the new value
    */
   protected void setValue(Map<P, Q> newValue)
   {
      if (!avoidMapLoop)
      {
         avoidMapLoop = true;
         Map<P, Q>  list = new HashMap<>();
         Map<P, Q> source = newValue;
         if (newValue instanceof MapProperty)
         {
            source = ((MapProperty<P,Q>)newValue).getValue();
         }
         for (P key : source.keySet())
         {
            list.put(key, source.get(key));
         }
         // for link purpose
         if (super.get() != map)
         {
            map.clear();
            map.putAll(list);
         }
         super.get().clear();
         super.get().putAll(list);
         fireChanged(list, this);
         avoidMapLoop = false;
      }
   }
   
   /**
    * Fire changed.
    *
    * @param oldList the old list
    * @param newList the new list
    */
   @Override
   protected void fireChanged(Map<P, Q> oldList, Map<P, Q> newList)
   {
      if (map != null)
      {
         super.fireChanged(oldList, newList);
         sizeProperty.set(newList.size());
      }
   }

   /**
    * Size property.
    *
    * @return the integer property
    */
   public IntegerProperty sizeProperty()
   {
      return sizeProperty;
   }

   /**
    * Size.
    *
    * @return the int
    */
   @Override
   public int size()
   {
      return super.get().size();
   }

   /**
    * Checks if is empty.
    *
    * @return true, if is empty
    */
   @Override
   public boolean isEmpty()
   {
      return super.get().isEmpty();
   }

   /**
    * Clear.
    */
   @Override
   public void clear()
   {
      if (isEditable())
      {
         // for link purpose
         if (super.get() != map)
         {
            map.clear();
         }
         super.get().clear();
         fireChanged(this, this);
      }
   }

   /**
    * Contains key.
    *
    * @param key the key
    * @return true, if successful
    */
   @Override
   public boolean containsKey(Object key)
   {
      return super.get().containsKey(key);
   }

   /**
    * Contains value.
    *
    * @param value the value
    * @return true, if successful
    */
   @Override
   public boolean containsValue(Object value)
   {
      return super.get().containsValue(value);
   }

   /**
    * Gets the value corresponding to the key.
    *
    * @param key the key
    * @return the q
    */
   @Override
   public Q get(Object key)
   {
      return super.get().get(key);
   }

   /**
    * Put a key/value in map.
    *
    * @param key the key
    * @param value the value
    * @return the q
    */
   @Override
   public Q put(P key, Q value)
   {
      Q ret = null;
      if (isEditable())
      {
         // for link purpose
         if (super.get() != map)
         {
            map.put(key, value);
         }
         ret = super.get().put(key, value);
         fireChanged(this, this);
      }
      return ret;
   }

   /**
    * Removes the key.
    *
    * @param key the key
    * @return the q
    */
   @Override
   public Q remove(Object key)
   {
      Q ret = null;
      if (isEditable())
      {
         // for link purpose
         if (super.get() != map)
         {
            map.remove(key);
         }
         ret = super.get().remove(key);
         fireChanged(this, this);
      }
      return ret;
   }

   /**
    * Put all.
    *
    * @param map the map
    */
   @Override
   public void putAll(Map<? extends P, ? extends Q> map)
   {
      if (isEditable())
      {
         // for link purpose
         if (super.get() != this.map)
         {
            this.map.putAll(map);
         }
         super.get().putAll(map);
         fireChanged(this, this);
      }
   }

   /**
    * Get key set.
    *
    * @return the sets the
    */
   @Override
   public Set<P> keySet()
   {
      return super.get().keySet();
   }

   /**
    * Get all values.
    *
    * @return the collection
    */
   @Override
   public Collection<Q> values()
   {
      return super.get().values();
   }

   /**
    * Get entry set.
    *
    * @return the sets the
    */
   @Override
   public Set<Entry<P, Q>> entrySet()
   {
      return get().entrySet();
   }
}
