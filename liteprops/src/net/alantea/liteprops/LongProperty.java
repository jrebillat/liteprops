package net.alantea.liteprops;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class LongProperty.
 */
public class LongProperty extends Property<Long>
{
   /** The _logger. */
   private static Logger logger = LoggerFactory.getLogger(LongProperty.class);
   
   /**
    * Instantiates a new long property.
    */
   public LongProperty()
   {
      super(0L, true);
   }
   
   /**
    * Instantiates a new long property.
    *
    * @param flag the flag
    */
   protected LongProperty(boolean flag)
   {
      super(0L, flag);
   }

   /**
    * Instantiates a new long property.
    *
    * @param value the value
    */
   public LongProperty(int value)
   {
      super((long)value, true);
   }

   /**
    * Instantiates a new long property.
    *
    * @param value the value
    */
   public LongProperty(long value)
   {
      super(value, true);
   }
   
   /**
    * Instantiates a new long property.
    *
    * @param value the value
    * @param flag the flag
    */
   protected LongProperty(long value, boolean flag)
   {
      super(value, flag);
   }

   /**
    * Add.
    *
    * @param property the property
    * @return the read only property
    */
   public LongProperty add(Property<?> property)
   {
      long bret = getLongFrom(this) + getLongFrom(property);
      LongProperty ret = new LongProperty(false);
      addListener((o, n) -> ret.setValue(getLongFrom(this) +  getLongFrom(property)));
      property.addListener((o, n) -> ret.setValue(getLongFrom(this) +  getLongFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * minus.
    *
    * @param property the property
    * @return the read only property
    */
   public LongProperty minus(Property<?> property)
   {
      long bret = getLongFrom(this) - getLongFrom(property);
      LongProperty ret = new LongProperty(false);
      addListener((o, n) -> ret.setValue(getLongFrom(this) -  getLongFrom(property)));
      property.addListener((o, n) -> ret.setValue(getLongFrom(this) -  getLongFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Divide.
    *
    * @param property the property
    * @return the read only property
    */
   public LongProperty divide(Property<?> property)
   {
      long bret = getLongFrom(this) / getLongFrom(property);
      LongProperty ret = new LongProperty(false);
      addListener((o, n) -> ret.setValue(getLongFrom(this) /  getLongFrom(property)));
      property.addListener((o, n) -> ret.setValue(getLongFrom(this) /  getLongFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Multiply.
    *
    * @param property the property
    * @return the read only property
    */
   public LongProperty multiply(Property<?> property)
   {
      long bret = getLongFrom(this) * getLongFrom(property);
      LongProperty ret = new LongProperty(false);
      addListener((o, n) -> ret.setValue(getLongFrom(this) *  getLongFrom(property)));
      property.addListener((o, n) -> ret.setValue(getLongFrom(this) *  getLongFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Minimum.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static LongProperty minimum(Property<?> property1, Property<?> property2)
   {
      LongProperty ret = new LongProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(Math.min(getLongFrom(property1), getLongFrom(property2)));
      }
      property1.addListener((o, n) -> ret.setValue(Math.min(getLongFrom(property1), getLongFrom(property2))));
      property2.addListener((o, n) -> ret.setValue(Math.min(getLongFrom(property1), getLongFrom(property2))));
      return ret;
   }
   
   /**
    * Maximum.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static LongProperty maximum(Property<?> property1, Property<?> property2)
   {
      LongProperty ret = new LongProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(Math.max(getLongFrom(property1), getLongFrom(property2)));
      }
      property1.addListener((o, n) -> ret.setValue(Math.max(getLongFrom(property1), getLongFrom(property2))));
      property2.addListener((o, n) -> ret.setValue(Math.max(getLongFrom(property1), getLongFrom(property2))));
      return ret;
   }
   
   /**
    * Gets the long from any kind of property.
    *
    * @param property the property
    * @return the long
    */
   public static long getLongFrom(Property<?> property)
   {
      Object content = property.get();
      if (content instanceof Integer)
      {
         return (Long)content;
      }
      else if (content instanceof Long)
      {
         return (Long)content;
      }
      else if (content instanceof Float)
      {
         return ((Float)content).longValue();
      }
      else if (content instanceof Boolean)
      {
         return ((Boolean)content) ? 1 : 0;
      }
      else if (content instanceof Double)
      {
         return ((Double)content).longValue();
      }
      else if (content instanceof String)
      {
         try
         {
            return Long.parseLong((String)content);
         }
         catch (NumberFormatException e)
         {
            logger.error("Error converting to long : " + content, e);
            return 0;
         }
      }
      return 0;
   }
}
