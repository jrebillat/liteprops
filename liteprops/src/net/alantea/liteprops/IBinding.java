package net.alantea.liteprops;

import java.util.List;

/**
 * The Interface IBinding.
 */
public interface IBinding
{
   
   /**
    * Gets the listeners.
    *
    * @return the listeners
    */
   public List<ChangeListener<Object>> getListeners();
   
   /**
    * Adds the listener.
    *
    * @param listener the listener
    */
   public default void addListener(ChangeListener<Object> listener)
   {
      List<ChangeListener<Object>> listeners = getListeners();
      if (!listeners.contains(listener))
      {
         listeners.add(listener);
      }
   }
   
   /**
    * Removes the listener.
    *
    * @param listener the listener
    */
   public default void removeListener(ChangeListener<Object> listener)
   {
      List<ChangeListener<Object>> listeners = getListeners();
      if (listeners.contains(listener))
      {
         listeners.remove(listener);
      }
   }


   /**
    * Fire the change.
    *
    * @param before the before
    * @param after the after
    */
   public default void fireChanged(Object before, Object after)
   {
      List<ChangeListener<Object>> listeners = getListeners();
      for (ChangeListener<Object> listener : listeners)
      {
         try
         {
            listener.changed(before, after);
         }
         catch (Exception e)
         {
            // silently ignore it
         }
      }
   }
}
