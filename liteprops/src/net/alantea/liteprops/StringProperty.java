package net.alantea.liteprops;

/**
 * The Class StringProperty.
 */
public class StringProperty extends Property<String>
{
   
   /**
    * Instantiates a new string property.
    */
   public StringProperty()
   {
      super("", true);
   }
   
   /**
    * Instantiates a new string property.
    *
    * @param flag the flag
    */
   protected StringProperty(boolean flag)
   {
      super("", flag);
   }
   
   /**
    * Instantiates a new string property.
    *
    * @param value the value
    */
   public StringProperty(String value)
   {
      super(value, true);
   }
   
   /**
    * Instantiates a new string property.
    *
    * @param value the value
    * @param flag the flag
    */
   protected StringProperty(String value, boolean flag)
   {
      super(value, flag);
   }

   /**
    * Concat.
    *
    * @param property the property
    * @return the read only property
    */
   public StringProperty concat(Property<?> property)
   {
      String bret = (String)get();
      StringProperty ret = new StringProperty(bret, false);
      addListener((o, n) -> ret.setValue((String)get() +  getStringFrom(property)));
      property.addListener((o, n) -> ret.setValue((String)get() +  getStringFrom(property)));
      
      return ret;
   }
   
   /**
    * Concat.
    *
    * @param suffix the suffix
    * @return the read only property
    */
   public StringProperty append(String suffix)
   {
      String bret = (String)get() + suffix;
      StringProperty ret = new StringProperty(bret, false);
      addListener((o, n) -> ret.setValue((String)get() + suffix));
      
      return ret;
   }
   
   /**
    * Concat.
    *
    * @param prefix the prefix
    * @return the read only property
    */
   public StringProperty prepend(String prefix)
   {
      String bret = prefix + (String)get();
      StringProperty ret = new StringProperty(bret, false);
      addListener((o, n) -> ret.setValue(prefix + (String)get()));
      
      return ret;
   }
   
   /**
    * Sub string.
    *
    * @param start start character
    * @param len length in characters
    * @return the read only property
    */
   public StringProperty substring(int start, int len)
   {
      String bret = (String)get().substring(start, start + len);
      StringProperty ret = new StringProperty(bret, false);
      addListener((o, n) -> ret.setValue((String)get().substring(start, start + len)));
      
      return ret;
   }
   
   /**
    * Sub string.
    *
    * @param start start character
    * @param len length in characters
    * @return the read only property
    */
   public StringProperty substring(IntegerProperty start, IntegerProperty len)
   {
      String bret = (String)get().substring(start.get(), start.get() + len.get());
      StringProperty ret = new StringProperty(bret, false);
      addListener((o, n) -> ret.setValue((String)get().substring(start.get(), start.get() + len.get())));
      start.addListener((o, n) -> ret.setValue((String)get().substring(start.get(), start.get() + len.get())));
      len.addListener((o, n) -> ret.setValue((String)get().substring(start.get(), start.get() + len.get())));
      
      return ret;
   }
   
   /**
    * Sub string.
    *
    * @param start start character
    * @param len length in characters
    * @return the read only property
    */
   public StringProperty substring(IntegerProperty start, int len)
   {
      String bret = (String)get().substring(start.get(), start.get() + len);
      StringProperty ret = new StringProperty(bret, false);
      addListener((o, n) -> ret.setValue((String)get().substring(start.get(), start.get() + len)));
      start.addListener((o, n) -> ret.setValue((String)get().substring(start.get(), start.get() + len)));
      
      return ret;
   }
   
   /**
    * Sub string.
    *
    * @param start start character
    * @param len length in characters
    * @return the read only property
    */
   public StringProperty substring(int start, IntegerProperty len)
   {
      String bret = (String)get().substring(start, start + len.get());
      StringProperty ret = new StringProperty(bret, false);
      addListener((o, n) -> ret.setValue((String)get().substring(start, start + len.get())));
      len.addListener((o, n) -> ret.setValue((String)get().substring(start, start + len.get())));
      
      return ret;
   }
   
   /**
    * Gets the String from a property.
    *
    * @param property the property
    * @return the string from
    */
   public String getStringFrom(Property<?> property)
   {
      Object content = property.get();
      if (content instanceof String)
      {
         return (String)content;
      }
      else if (content instanceof Integer)
      {
         return Integer.toString((Integer)content);
      }
      else if (content instanceof Long)
      {
         return Long.toString((Long)content);
      }
      else if (content instanceof Float)
      {
         return Float.toString((Float)content);
      }
      else if (content instanceof Double)
      {
         return Double.toString((Double)content);
      }
      else if (content instanceof Boolean)
      {
         return ((Boolean)content)? "true" : "false";
      }
      return "";
   }
}
