package net.alantea.liteprops;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The base Class Property.
 *
 * @param <T> the generic type
 */
public class Property<T>
{
   /** The _logger. */
   private static Logger logger = LoggerFactory.getLogger(Property.class);
   
   /** The listeners. */
   private transient List<ChangeListener<T>> listeners = new LinkedList<>();
   
   /** The value. */
   private T value;
   
   /** The elements it may be linked from. */
   private transient Property<T> linkedFrom;
   
   /** The elements it may be linked to. */
   private transient List<Property<T>> linkedTo = new LinkedList<>();
   
   /** The element is may be bound to. */
   private transient Property<T> bound;
   
   /** The avoid loop. */
   private boolean avoidLoop;
   
   /** The editable. */
   private boolean editable;

   /**
    * Instantiates a new property.
    */
   public Property()
   {
      editable = true;
   }
   /**
    * Instantiates a new property.
    *
    * @param value the value
    */
   public Property(T value)
   {
      editable = true;
      this.value = value;
   }
   
   /**
    * Instantiates a new property.
    *
    * @param editable the editable
    */
   protected Property(boolean editable)
   {
      this.editable = editable;
   }
   
   /**
    * Instantiates a new property.
    *
    * @param value the value
    * @param editable the editable
    */
   protected Property(T value, boolean editable)
   {
      this.editable = editable;
      this.value = value;
   }
   
   /**
    * Checks if is editable.
    *
    * @return true, if is editable
    */
   protected boolean isEditable()
   {
      return editable;
   }
   
   /**
    * Sets the value.
    *
    * @param newValue the new value
    * @return true, if successful
    */
   public boolean set(T newValue)
   {
      if ((linkedFrom() == null) && (editable))
      {
         setValue(newValue);
         return true;
      }
      return false;
   }
   
   /**
    * Adds the listener.
    *
    * @param listener the listener
    */
   public void addListener(ChangeListener<T> listener)
   {
      if (!listeners.contains(listener))
      {
         listeners.add(listener);
      }
   }
   
   /**
    * Removes the listener.
    *
    * @param listener the listener
    */
   public void removeListener(ChangeListener<T> listener)
   {
      if (listeners.contains(listener))
      {
         listeners.remove(listener);
      }
   }
   
   /**
    * Gets the value.
    *
    * @return the t
    */
   public T get()
   {
      if (linkedFrom != null)
      {
         return linkedFrom.get();
      }
      else
      {
         return value;
      }
   }
   
   /**
    * Linked from.
    *
    * @return the read only property
    */
   protected Property<?> linkedFrom()
   {
      return linkedFrom;
   }
   
   /**
    * Checks if is avoid loop.
    *
    * @return true, if is avoid loop
    */
   protected boolean isAvoidLoop()
   {
      return avoidLoop;
   }
   
   /**
    * Sets the avoid loop.
    *
    * @param flag the new avoid loop
    */
   protected void setAvoidLoop(boolean flag)
   {
      avoidLoop = flag;
   }

   /**
    * Sets the value.
    *
    * @param newValue the new value
    */
   protected void setValue(T newValue)
   {
      if (! avoidLoop)
      {
         avoidLoop = true;
         T oldValue = value;
         value = newValue;
         fireChanged(oldValue, newValue);
         avoidLoop = false;
      }
   }
   
   /**
    * Gets the value.
    *
    * @return the value
    */
   protected T getValue()
   {
      return value;
   }

   /**
    * Fire the change process.
    *
    */
   public void fireChanged()
   {
      fireChanged(value, value);
   }

   /**
    * Fire the change.
    *
    * @param oldValue the old value
    * @param newValue the new value
    */
   protected void fireChanged(T oldValue, T newValue)
   {
      for (ChangeListener<T> listener : listeners)
      {
         try
         {
            listener.changed(oldValue, newValue);
         }
         catch (Exception e)
         {
            logger.error("Error calling listener", e);
         }
      }

      for (Property<T> link : linkedTo)
      {
         link.setValue(newValue);
      }

      if ((bound != null) && (bound.value != newValue) && (! bound.avoidLoop))
      {
         avoidLoop = true;
         bound.setValue(newValue);
         avoidLoop = false;
      }
      else
      {
         setValue(newValue);
      }
   }

   /**
    * Link.
    *
    * @param origin the origin
    * @return true, if successful
    */
   public boolean link(Property<T> origin)
   {
      if ((origin != null) && (bound == null))
      {
         linkedFrom = origin;
         origin.linkTo(this);
         setValue(linkedFrom.value);
         return true;
      }
      return false;
   }
   
   /**
    * Link to.
    *
    * @param destination the destination
    */
   private void linkTo(Property<T> destination)
   {
      if (!linkedTo.contains(destination))
      {
         linkedTo.add(destination);
      }
   }
   
   /**
    * Unlink.
    */
   public void unlink()
   {
      if (linkedFrom != null)
      {
         linkedFrom.linkedTo.remove(this);
         linkedFrom = null;
      }
   }
   
   /**
    * Checks if is linked.
    *
    * @return true, if is linked
    */
   public boolean isLinked()
   {
      return linkedFrom != null;
   }
   
   /**
    * Bind.
    *
    * @param duplicate the duplicate
    * @return true, if successful
    */
   public boolean bind(Property<T> duplicate)
   {
      boolean doIt = false;
      if ((bound == null) && (duplicate.bound == null))
      {
         setValue(duplicate.value);
         doIt = true;
      }
      else if ((bound == null) && (duplicate.bound != this))
      {
         duplicate.unbind();
         setValue(duplicate.value);
         doIt = true;
      }
      else if ((bound == null) && (duplicate.bound == this))
      {
         doIt = true;
      }
      if (doIt)
      {
         bound = duplicate;
         duplicate.bound = this;
         doIt = true;
      }
      return doIt;
   }
   
   /**
    * Unbind.
    */
   public void unbind()
   {
      if (bound != null)
      {
         bound.bound = null;
         bound = null;
      }
   }
   
   /**
    * Checks if is bound.
    *
    * @return true, if is bound
    */
   public boolean isBound()
   {
      return bound != null;
   }
}
