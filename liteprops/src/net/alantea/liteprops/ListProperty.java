package net.alantea.liteprops;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * The Class ListProperty.
 *
 * @param <P> the generic type for the list
 */
public class ListProperty<P> extends Property<List<P>> implements List<P>
{
   
   /** The size property. */
   private IntegerProperty sizeProperty = new IntegerProperty();
   
   /** The avoid list loop. */
   private boolean avoidListLoop;
   
   /** The list. */
   private LinkedList<P> list;
   
   /**
    * Instantiates a new list property.
    */
   public ListProperty()
   {
      super(new LinkedList<>());
      list = (LinkedList<P>) super.get();
      sizeProperty.set(0);
   }

   /**
    * Instantiates a new list property.
    *
    * @param value the value
    */
   public ListProperty(List<P> value)
   {
      this();
      setValue(value);
   }
   
   /**
    * Gets the list.
    *
    * @return the list
    */
   public List<P> get()
   {
      return this;
   }
   
   /**
    * Sets a new list as value.
    *
    * @param newValue the new value
    */
   protected void setValue(List<P> newValue)
   {
      List<P> buffer = new LinkedList<>();
      buffer.addAll(newValue);
      if ((!avoidListLoop) && (!isAvoidLoop()))
      {
         setAvoidLoop(true);
         avoidListLoop = true;
         java.util.List<P> buffer1 = new LinkedList<>();
         buffer1.addAll(newValue);
         buffer1.removeIf(list::contains);
         java.util.List<P> buffer2 = new LinkedList<>();
         buffer2.addAll(list);
         buffer2.removeIf(newValue::contains);
         if ((buffer1.size() > 0) || (buffer2.size() > 0))
         {
            list.clear();
            list.addAll(buffer);
            fireChanged(null, list);
         }
         avoidListLoop = false;
         setAvoidLoop(false);
      }
   }
   
   /**
    * Fire changed.
    *
    * @param oldList the old list
    * @param newList the new list
    */
   @Override
   protected void fireChanged(List<P> oldList, List<P> newList)
   {
      sizeProperty.set(newList.size());
      super.fireChanged(oldList, newList);
   }

   /**
    * Size property.
    *
    * @return the integer property
    */
   public IntegerProperty sizeProperty()
   {
      return sizeProperty;
   }

   /**
    * Size.
    *
    * @return the size  s an integer
    */
   @Override
   public int size()
   {
      return list.size();
   }

   /**
    * Checks if the list is empty.
    *
    * @return true, if is empty
    */
   @Override
   public boolean isEmpty()
   {
      return list.isEmpty();
   }

   /**
    * Checks if the list contains an object.
    *
    * @param o the o
    * @return true, if successful
    */
   @Override
   public boolean contains(Object o)
   {
      return list.contains(o);
   }

   /**
    * Iterator on the list.
    *
    * @return the iterator
    */
   @Override
   public Iterator<P> iterator()
   {
      return list.iterator();
   }

   /**
    * Convert value to array of objects.
    *
    * @return the object[]
    */
   @Override
   public Object[] toArray()
   {
      return list.toArray();
   }

   /**
    * Convert value to array of som kind.
    *
    * @param <T> the generic type
    * @param a the a
    * @return the t[]
    */
   @Override
   public <T> T[] toArray(T[] a)
   {
      return list.toArray(a);
   }

   /**
    * Adds the object at the end of the list.
    *
    * @param e the e
    * @return true, if successful
    */
   @Override
   public boolean add(P e)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         boolean ret =  list.add(e);
         fireChanged(null, list);
         return ret;
      }
      return false;
   }

   /**
    * Removes the object if it is in the list.
    *
    * @param o the o
    * @return true, if successful
    */
   @Override
   public boolean remove(Object o)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         boolean ret =  list.remove(o);
         fireChanged(null, list);
         return ret;
      }
      return false;
   }

   /**
    * Test if list contains all elements.
    *
    * @param c the c
    * @return true, if successful
    */
   @Override
   public boolean containsAll(Collection<?> c)
   {
      boolean ret =  list.containsAll(c);
      fireChanged(null, list);
      return ret;
   }

   /**
    * Adds all the elements to the list.
    *
    * @param c the c
    * @return true, if successful
    */
   @Override
   public boolean addAll(Collection<? extends P> c)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         boolean ret =  list.addAll(c);
         fireChanged(null, list);
         return ret;
      }
      return false;
   }

   /**
    * Removes all given elements from the list.
    *
    * @param c the c
    * @return true, if successful
    */
   @Override
   public boolean removeAll(Collection<?> c)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         boolean ret = list.removeAll(c);
         fireChanged(null, list);
         return ret;
      }
      return false;
   }

   /**
    * Retain all elements corresponding to given collection.
    *
    * @param c the c
    * @return true, if successful
    */
   @Override
   public boolean retainAll(Collection<?> c)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         boolean ret = list.retainAll(c);
         fireChanged(null, list);
         return ret;
      }
      return false;
   }

   /**
    * Clear list.
    */
   @Override
   public void clear()
   {
      if ((isEditable()) && (!avoidListLoop) && (!isAvoidLoop()))
      {
         avoidListLoop = true;
         list.clear();
         fireChanged(null, list);
         avoidListLoop = false;
      }
   }

   /**
    * Adds all given elements, starting at the given position.
    *
    * @param index the index
    * @param c the c
    * @return true, if successful
    */
   @Override
   public boolean addAll(int index, Collection<? extends P> c)
   {
      if ((isEditable()) && (!avoidListLoop) && (!isAvoidLoop()))
      {
         avoidListLoop = true;
         boolean ret = list.addAll(index, c);
         fireChanged(null, list);
         avoidListLoop = false;
         return ret;
      }
      return false;
   }

   /**
    * Sets the list to all given elements.
    *
    * @param c the new all
    */
   public void setAll(Collection<? extends P> c)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         list.clear();
         list.addAll(c);
         fireChanged(null, list);
      }
   }

   /**
    * Gets the element at given index.
    *
    * @param index the index
    * @return the p
    */
   @Override
   public P get(int index)
   {
      return list.get(index);
   }

   /**
    * Sets the element at given index.
    *
    * @param index the index
    * @param element the element
    * @return the p
    */
   @Override
   public P set(int index, P element)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         P ret = list.set(index, element);
         fireChanged(null, list);
         return ret;
      }
      return null;
   }

   /**
    * Adds the element at given index.
    *
    * @param index the index
    * @param element the element
    */
   @Override
   public void add(int index, P element)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         list.add(index, element);
         fireChanged(null, list);
      }
   }

   /**
    * Removes the element from given index.
    *
    * @param index the index
    * @return the p
    */
   @Override
   public P remove(int index)
   {
      if (isEditable() && (!avoidListLoop) && (!isAvoidLoop()))
      {
         P ret = list.remove(index);
         fireChanged(null, list);
         return ret;
      }
      return null;
   }

   /**
    * Index of object in list.
    *
    * @param o the o
    * @return the int
    */
   @Override
   public int indexOf(Object o)
   {
      return list.indexOf(o);
   }

   /**
    * Last index of object in list.
    *
    * @param o the o
    * @return the index
    */
   @Override
   public int lastIndexOf(Object o)
   {
      return list.lastIndexOf(o);
   }

   /**
    * List iterator.
    *
    * @return the list iterator
    */
   @Override
   public ListIterator<P> listIterator()
   {
      return list.listIterator();
   }

   /**
    * List iterator.
    *
    * @param index the index
    * @return the list iterator
    */
   @Override
   public ListIterator<P> listIterator(int index)
   {
      return list.listIterator(index);
   }

   /**
    * Sub list.
    *
    * @param fromIndex the from index
    * @param toIndex the to index
    * @return the list
    */
   @Override
   public List<P> subList(int fromIndex, int toIndex)
   {
      return list.subList(fromIndex, toIndex);
   }

}
