package net.alantea.liteprops;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DoubleProperty.
 */
public class DoubleProperty extends Property<Double>
{
   /** The _logger. */
   private static Logger logger = LoggerFactory.getLogger(DoubleProperty.class);
   
   /**
    * Instantiates a new double property set to 0.0.
    */
   public DoubleProperty()
   {
      super(0.0, true);
   }
   
   /**
    * Instantiates a new double property set to 0.0 that may be editable.
    *
    * @param flag the flag
    */
   protected DoubleProperty(boolean flag)
   {
      super(0.0, flag);
   }
   
   /**
    * Instantiates a new double property.
    *
    * @param value the value
    */
   public DoubleProperty(float value)
   {
      super((double)value, true);
   }
   
   /**
    * Instantiates a new double property.
    *
    * @param value the value
    */
   public DoubleProperty(double value)
   {
      super(value, true);
   }
   
   /**
    * Instantiates a new double property.
    *
    * @param value the value
    * @param flag the flag
    */
   protected DoubleProperty(double value, boolean flag)
   {
      super(value, flag);
   }

   /**
    * Add.
    *
    * @param property the property
    * @return the read only property
    */
   public DoubleProperty add(Property<?> property)
   {
      double bret = getDoubleFrom(this) + getDoubleFrom(property);
      DoubleProperty ret = new DoubleProperty(false);
      addListener((o, n) -> ret.setValue(getDoubleFrom(this) +  getDoubleFrom(property)));
      property.addListener((o, n) -> ret.setValue(getDoubleFrom(this) +  getDoubleFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * minus.
    *
    * @param property the property
    * @return the read only property
    */
   public DoubleProperty minus(Property<?> property)
   {
      double bret = getDoubleFrom(this) - getDoubleFrom(property);
      DoubleProperty ret = new DoubleProperty(false);
      addListener((o, n) -> ret.setValue(getDoubleFrom(this) -  getDoubleFrom(property)));
      property.addListener((o, n) -> ret.setValue(getDoubleFrom(this) -  getDoubleFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Divide.
    *
    * @param property the property
    * @return the read only property
    */
   public DoubleProperty divide(Property<?> property)
   {
      double bret = getDoubleFrom(this) / getDoubleFrom(property);
      DoubleProperty ret = new DoubleProperty(false);
      addListener((o, n) -> ret.setValue(getDoubleFrom(this) /  getDoubleFrom(property)));
      property.addListener((o, n) -> ret.setValue(getDoubleFrom(this) /  getDoubleFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Multiply.
    *
    * @param property the property
    * @return the read only property
    */
   public DoubleProperty multiply(Property<?> property)
   {
      double bret = getDoubleFrom(this) * getDoubleFrom(property);
      DoubleProperty ret = new DoubleProperty(false);
      addListener((o, n) -> ret.setValue(getDoubleFrom(this) *  getDoubleFrom(property)));
      property.addListener((o, n) -> ret.setValue(getDoubleFrom(this) *  getDoubleFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Minimum.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static DoubleProperty minimum(Property<?> property1, Property<?> property2)
   {
      DoubleProperty ret = new DoubleProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(Math.min(getDoubleFrom(property1), getDoubleFrom(property2)));
      }
      property1.addListener((o, n) -> ret.setValue(Math.min(getDoubleFrom(property1), getDoubleFrom(property2))));
      property2.addListener((o, n) -> ret.setValue(Math.min(getDoubleFrom(property1), getDoubleFrom(property2))));
      return ret;
   }
   
   /**
    * Maximum.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static DoubleProperty maximum(Property<?> property1, Property<?> property2)
   {
      DoubleProperty ret = new DoubleProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(Math.max(getDoubleFrom(property1), getDoubleFrom(property2)));
      }
      property1.addListener((o, n) -> ret.setValue(Math.max(getDoubleFrom(property1), getDoubleFrom(property2))));
      property2.addListener((o, n) -> ret.setValue(Math.max(getDoubleFrom(property1), getDoubleFrom(property2))));
      return ret;
   }
   
   /**
    * Gets the double from any kind of property.
    *
    * @param property the property
    * @return the double
    */
   public static double getDoubleFrom(Property<?> property)
   {
      Object content = property.get();
      if (content instanceof Double)
      {
         return (Double)content;
      }
      else if (content instanceof Boolean)
      {
         return ((Boolean)content) ? 1.0 : 0.0;
      }
      else if (content instanceof Integer)
      {
         return ((Integer)content).intValue();
      }
      else if (content instanceof Long)
      {
         return ((Long)content).doubleValue();
      }
      else if (content instanceof Float)
      {
         return ((Float)content).doubleValue();
      }
      else if (content instanceof String)
      {
         try
         {
            return Double.parseDouble((String)content);
         }
         catch (NumberFormatException e)
         {
            logger.error("Error converting to double : " + content, e);
            return 0.0;
         }
      }
      return 0.0;
   }
}
