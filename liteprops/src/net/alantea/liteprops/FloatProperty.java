package net.alantea.liteprops;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FloatProperty.
 */
public class FloatProperty extends Property<Float>
{
   /** The _logger. */
   private static Logger logger = LoggerFactory.getLogger(DoubleProperty.class);
   
   /**
    * Instantiates a new float property.
    */
   public FloatProperty()
   {
      super(0.0f, true);
   }
   
   /**
    * Instantiates a new float property.
    *
    * @param flag the flag
    */
   protected FloatProperty(boolean flag)
   {
      super(0.0f, flag);
   }

   /**
    * Instantiates a new float property.
    *
    * @param value the value
    */
   public FloatProperty(float value)
   {
      super(value, true);
   }
   
   /**
    * Instantiates a new float property.
    *
    * @param value the value
    * @param flag the flag
    */
   protected FloatProperty(float value, boolean flag)
   {
      super(value, flag);
   }

   /**
    * Add.
    *
    * @param property the property
    * @return the read only property
    */
   public FloatProperty add(Property<?> property)
   {
      float bret = getFloatFrom(this) + getFloatFrom(property);
      FloatProperty ret = new FloatProperty(false);
      addListener((o, n) -> ret.setValue(getFloatFrom(this) +  getFloatFrom(property)));
      property.addListener((o, n) -> ret.setValue(getFloatFrom(this) +  getFloatFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * minus.
    *
    * @param property the property
    * @return the read only property
    */
   public FloatProperty minus(Property<?> property)
   {
      float bret = getFloatFrom(this) - getFloatFrom(property);
      FloatProperty ret = new FloatProperty(false);
      addListener((o, n) -> ret.setValue(getFloatFrom(this) -  getFloatFrom(property)));
      property.addListener((o, n) -> ret.setValue(getFloatFrom(this) -  getFloatFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Divide.
    *
    * @param property the property
    * @return the read only property
    */
   public FloatProperty divide(Property<?> property)
   {
      float bret = getFloatFrom(this) / getFloatFrom(property);
      FloatProperty ret = new FloatProperty(false);
      addListener((o, n) -> ret.setValue(getFloatFrom(this) /  getFloatFrom(property)));
      property.addListener((o, n) -> ret.setValue(getFloatFrom(this) /  getFloatFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Multiply.
    *
    * @param property the property
    * @return the read only property
    */
   public FloatProperty multiply(Property<?> property)
   {
      float bret = getFloatFrom(this) * getFloatFrom(property);
      FloatProperty ret = new FloatProperty(false);
      addListener((o, n) -> ret.setValue(getFloatFrom(this) *  getFloatFrom(property)));
      property.addListener((o, n) -> ret.setValue(getFloatFrom(this) *  getFloatFrom(property)));
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * Minimum.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static FloatProperty minimum(Property<?> property1, Property<?> property2)
   {
      FloatProperty ret = new FloatProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(Math.min(getFloatFrom(property1), getFloatFrom(property2)));
      }
      property1.addListener((o, n) -> ret.setValue(Math.min(getFloatFrom(property1), getFloatFrom(property2))));
      property2.addListener((o, n) -> ret.setValue(Math.min(getFloatFrom(property1), getFloatFrom(property2))));
      return ret;
   }
   
   /**
    * Maximum.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static FloatProperty maximum(Property<?> property1, Property<?> property2)
   {
      FloatProperty ret = new FloatProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(Math.max(getFloatFrom(property1), getFloatFrom(property2)));
      }
      property1.addListener((o, n) -> ret.setValue(Math.max(getFloatFrom(property1), getFloatFrom(property2))));
      property2.addListener((o, n) -> ret.setValue(Math.max(getFloatFrom(property1), getFloatFrom(property2))));
      return ret;
   }
   
   /**
    * Gets a float from any kind of property.
    *
    * @param property the property
    * @return the double
    */
   public static float getFloatFrom(Property<?> property)
   {
      Object content = property.get();
      if (content instanceof Double)
      {
         return (Float)content;
      }
      else if (content instanceof Boolean)
      {
         return ((Boolean)content) ? 1.0f : 0.0f;
      }
      else if (content instanceof Integer)
      {
         return ((Integer)content).intValue();
      }
      else if (content instanceof Long)
      {
         return ((Long)content).floatValue();
      }
      else if (content instanceof Float)
      {
         return ((Float)content).floatValue();
      }
      else if (content instanceof String)
      {
         try
         {
            return Float.parseFloat((String)content);
         }
         catch (NumberFormatException e)
         {
            logger.error("Error converting to float : " + content, e);
            return 0.0f;
         }
      }
      return 0.0f;
   }
}
