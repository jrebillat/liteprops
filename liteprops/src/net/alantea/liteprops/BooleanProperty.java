package net.alantea.liteprops;

/**
 * The Class BooleanProperty.
 */
public class BooleanProperty extends Property<Boolean>
{
   
   /**
    * Instantiates a new editable boolean property set as false.
    */
   public BooleanProperty()
   {
      super(false, true);
   }
   
   /**
    * Instantiates a new editable boolean property.
    *
    * @param value the value
    */
   public BooleanProperty(boolean value)
   {
      super(value, true);
   }
   
   /**
    * Instantiates a new boolean property.
    *
    * @param value the value
    * @param flag the flag to indicate whether it is editable
    */
   protected BooleanProperty(boolean value, boolean flag)
   {
      super(value, flag);
   }
   
   /**
    * New BooleanProperty that will always be the opposite of this property.
    *
    * @return the read only property
    */
   public BooleanProperty not()
   {
      BooleanProperty ret = new BooleanProperty(!getBooleanFrom(this), false);
      addListener((o, n) -> ret.setValue(!(getBooleanFrom(this))));
      return ret;
   }

   /**
    * New BooleanProperty that will always be equals to a boolean "and" operation of this property and the given one.
    *
    * @param property the other property
    * @return the read only property
    */
   public BooleanProperty and(Property<?> property)
   {
      boolean bret = false;
      if (getBooleanFrom(this))
      {
         bret = getBooleanFrom(property);
      }
      BooleanProperty ret = new BooleanProperty(false, false);
      addListener((o, n) ->
      {
         ret.setValue(getBooleanFrom(this) &&  getBooleanFrom(property));
      });
      property.addListener((o, n) -> ret.setValue(getBooleanFrom(this) &&  getBooleanFrom(property)));
      
      ret.setValue(bret);
      return ret;
   }
   
   /**
    * New BooleanProperty that will always be equals to a boolean "or" operation of this property and the given one.
    *
    * @param property the property
    * @return the read only property
    */
   public BooleanProperty or(Property<?> property)
   {
      boolean bret = true;
      if (!getBooleanFrom(this))
      {
         bret = getBooleanFrom(property);
      }
      BooleanProperty ret = new BooleanProperty(false, false);
      addListener((o, n) -> ret.setValue(getBooleanFrom(this) ||  getBooleanFrom(property)));
      property.addListener((o, n) -> ret.setValue(getBooleanFrom(this) ||  getBooleanFrom(property)));

      ret.setValue(bret);
      return ret;
   }
   
   /**
    * New BooleanProperty that will always be equals to a boolean "nor" operation of this property and the given one.
    *
    * @param property the property
    * @return the read only property
    */
   public BooleanProperty nor(Property<?> property)
   {
      boolean bret = false;
      if (!getBooleanFrom(this))
      {
         bret = !getBooleanFrom(property);
      }
      BooleanProperty ret = new BooleanProperty(false, false);
      ret.setValue(bret);
      addListener((o, n) -> ret.setValue(!(getBooleanFrom(this) ||  getBooleanFrom(property))));
      property.addListener((o, n) -> ret.setValue(!(getBooleanFrom(this) ||  getBooleanFrom(property))));
      return ret;
   }
   
   /**
    * New BooleanProperty that will always be equals to a boolean "nand" operation of this property and the given one.
    *
    * @param property the property
    * @return the read only property
    */
   public BooleanProperty nand(Property<?> property)
   {
      boolean bret = false;
      if (getBooleanFrom(this))
      {
         bret = getBooleanFrom(property);
      }
      BooleanProperty ret = new BooleanProperty(false, false);
      ret.setValue(bret);
      addListener((o, n) -> ret.setValue(!(getBooleanFrom(this) &&  getBooleanFrom(property))));
      property.addListener((o, n) -> ret.setValue(!(getBooleanFrom(this) &&  getBooleanFrom(property))));
      return ret;
   }
   
   /**
    * New BooleanProperty that will always be equals to a boolean "equals" operation of this property and the given one.
    *
    * @param property1 the property 1
    * @param property2 the property 2
    * @return the read only property
    */
   public static BooleanProperty equals(Property<?> property1, Property<?> property2)
   {
      BooleanProperty ret = new BooleanProperty(false);
      if ((property1.get() != null) && (property2.get() != null))
      {
         ret.setValue(property1.get().equals(property2.get()));
      }
      property1.addListener((o, n) -> ret.setValue(property1.get().equals(property2.get())));
      property2.addListener((o, n) -> ret.setValue(property1.get().equals(property2.get())));
      return ret;
   }
   
   /**
    * Gets the boolean from a property.
    *
    * @param property the property
    * @return the boolean from
    */
   public static boolean getBooleanFrom(Property<?> property)
   {
      Object content = property.get();
      if (content instanceof Boolean)
      {
         return (Boolean)content;
      }
      else if (content instanceof Integer)
      {
         return (Integer)content != 0;
      }
      else if (content instanceof Double)
      {
         return (Double)content != 0.0;
      }
      else if (content instanceof Long)
      {
         return (Long)content != 0;
      }
      else if (content instanceof Float)
      {
         return (Float)content != 0.0;
      }
      else if (content instanceof String)
      {
         return ((String)content).equalsIgnoreCase("true");
      }
      return false;
   }
}
