package net.alantea.liteprops.tree;

/**
 * The Class PropTree.
 *
 * @param <P> the generic type
 * @param <Q> the generic type
 */
public class TreePropertyTree<P, Q>
{
      
      /** The root. */
      TreePropertyNode<P, Q> root;

      /**
       * Instantiates a new prop tree.
       */
      public TreePropertyTree()
      {
      }

      /**
       * Gets the root.
       *
       * @return the root
       */
      TreePropertyNode<P, Q> getRoot()
      {
         return root;
      }

      /**
       * Sets the root.
       *
       * @param root the root
       */
      void setRoot(TreePropertyNode<P, Q> root)
      {
         this.root = root;
      }
      
}
