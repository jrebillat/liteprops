package net.alantea.liteprops.tree;

/**
 * The Class PropLeaf.
 *
 * @param <P> the generic type
 * @param <Q> the generic type
 */
public class TreePropertyLeaf<P, Q>
{
   
   /** The data. */
   Q data;
   
   /** The parent. */
   TreePropertyNode<P, Q> parent;
   
   /**
    * Gets the data.
    *
    * @return the data
    */
   Q getData()
   {
      return data;
   }
   
   /**
    * Sets the data.
    *
    * @param data the new data
    */
   void setData(Q data)
   {
      this.data = data;
   }
   
   /**
    * Gets the parent.
    *
    * @return the parent
    */
   TreePropertyNode<P, Q> getParent()
   {
      return parent;
   }
   
   /**
    * Sets the parent.
    *
    * @param parent the parent
    */
   void setParent(TreePropertyNode<P, Q> parent)
   {
      this.parent = parent;
   }
}