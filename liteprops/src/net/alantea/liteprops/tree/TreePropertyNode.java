package net.alantea.liteprops.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class PropNode.
 *
 * @param <P> the generic type
 * @param <Q> the generic type
 */
public class TreePropertyNode<P, Q>
{
   
   /** The data. */
   P data;
   
   /** The parent. */
   TreePropertyNode<P, Q> parent;
   
   /** The children. */
   List<TreePropertyNode<P, Q>> children = new ArrayList<TreePropertyNode<P, Q>>();
   
   /** The leafs. */
   List<TreePropertyLeaf<P, Q>> leafs = new ArrayList<TreePropertyLeaf<P, Q>>();
   
   /**
    * Gets the data.
    *
    * @return the data
    */
   P getData()
   {
      return data;
   }
   
   /**
    * Sets the data.
    *
    * @param data the new data
    */
   void setData(P data)
   {
      this.data = data;
   }
   
   /**
    * Gets the parent.
    *
    * @return the parent
    */
   TreePropertyNode<P, Q> getParent()
   {
      return parent;
   }
   
   /**
    * Sets the parent.
    *
    * @param parent the parent
    */
   void setParent(TreePropertyNode<P, Q> parent)
   {
      this.parent = parent;
   }
   
   /**
    * Gets the children.
    *
    * @return the children
    */
   List<TreePropertyNode<P, Q>> getChildren()
   {
      return children;
   }
   
   /**
    * Sets the children.
    *
    * @param children the children
    */
   void setChildren(List<TreePropertyNode<P, Q>> children)
   {
      this.children = children;
   }
   
   /**
    * Clears the children.
    *
    */
   void clearChildren()
   {
      children.clear();
   }
   
   /**
    * Gets the leafs.
    *
    * @return the leafs
    */
   List<TreePropertyLeaf<P, Q>> getLeafs()
   {
      return leafs;
   }
   
   /**
    * Sets the leafs.
    *
    * @param leafs the leafs
    */
   void setLeafs(List<TreePropertyLeaf<P, Q>> leafs)
   {
      this.leafs = leafs;
   }
   
   /**
    * Clears the leafs.
    *
    */
   void clearLeafs()
   {
      leafs.clear();
   }

   public void add(TreePropertyNode<P, Q> node)
   {
      children.add(node);
   }

   public void remove(TreePropertyNode<P, Q> node)
   {
      children.remove(node);
   }

   public void add(TreePropertyLeaf<P, Q> node)
   {
      leafs.add(node);
   }

   public void remove(TreePropertyLeaf<P, Q> node)
   {
      leafs.remove(node);
   }
}