package net.alantea.liteprops.tree;

import java.util.List;

import net.alantea.liteprops.Property;

/**
 * The Class TreeProperty.
 *
 * @param <P> the generic type
 * @param <Q> the generic type
 */
public class TreeProperty<P, Q> extends Property<TreePropertyTree<P, Q>>
{
   
   /** The root. */
   private TreePropertyNode<P, Q> root;
   
   /**
    * Instantiates a new tree property.
    */
   public TreeProperty()
   {
      super(new TreePropertyTree<P, Q>());
   }
   
   /**
    * Gets the root.
    *
    * @return the root
    */
   protected TreePropertyNode<P, Q> getRoot()
   {
     return root;
   }

   /**
    * Checks if is empty.
    *
    * @return true, if is empty
    */
   public boolean isEmpty()
   {
      return root == null;
   }

   /**
    * Clear.
    */
   public void clear()
   {
      if (isEditable())
      {
         root = null;
         fireChanged(get(), get());
      }
   }

   /**
    * Clear children.
    *
    * @param node the node
    */
   public void clearChildren(TreePropertyNode<P, Q> node)
   {
      node.clearChildren();
      fireChanged(get(), get());
   }

   /**
    * Clear leafs.
    *
    * @param node the node
    */
   public void clearLeafs(TreePropertyNode<P, Q> node)
   {
      node.clearLeafs();
      fireChanged(get(), get());
   }
   
   /**
    * Gets the children.
    *
    * @param node the node
    * @return the children
    */
   public List<TreePropertyNode<P, Q>> getChildren(TreePropertyNode<P, Q> node)
   {
      return node.getChildren();
   }

   /**
    * Gets the value.
    *
    * @param node the node
    * @return the value
    */
   public P getValue(TreePropertyNode<P, Q> node)
   {
      return node.getData();
   }
   
   /**
    * Adds the node.
    *
    * @param value the value
    * @return the tree property node
    */
   public TreePropertyNode<P, Q> addNode(P value)
   {
      TreePropertyNode<P, Q> ret = new TreePropertyNode<P, Q>();
      ret.setData(value);
      fireChanged(get(), get());
      return ret;
   }
   
   /**
    * Gets the node.
    *
    * @param parent the parent
    * @param value the value
    * @return the node
    */
   public TreePropertyNode<P, Q> getNode(TreePropertyNode<P, Q> parent, P value)
   {
      List<TreePropertyNode<P, Q>> list = parent.getChildren();
      for (TreePropertyNode<P, Q> child : list)
      {
         if (child.equals(value))
         {
            return child;
         }
      }
      return null;
   }
   
   /**
    * Removes the node.
    *
    * @param parent the parent
    * @param value the value
    */
   public void removeNode(TreePropertyNode<P, Q> parent, P value)
   {
      TreePropertyNode<P, Q> node = getNode(parent, value);
      if (node != null)
      {
         parent.remove(node);
         fireChanged(get(), get());
      }
   }
}
