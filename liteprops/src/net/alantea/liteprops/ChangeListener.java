package net.alantea.liteprops;

/**
 * The listener interface for receiving change events.
 * The class that is interested in processing a change
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addChangeListener</code> method. When
 * the change event occurs, that object's appropriate
 * method is invoked.
 *
 * @param <T> the generic type
 */
@FunctionalInterface
public interface ChangeListener<T>
{
    /**
     * Changed.
     *
     * @param oldValue the old value
     * @param newValue the new value
     */
    public void  changed(T oldValue, T newValue);
}
