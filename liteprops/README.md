# Liteprops
This package contains a liter version of the idea behind JavaFX properties mechanism.

## Overview
This package contains classes to manage bound properties. A bound property is an instance of a Property-derived class that contains some kind of object and trigger actions when the object changes.

If you want something really wide and offering many possibilities, use the JavaFX Properties instead. Liteprops is lite. It just manage properties, property change listeners and links between properties.

## What is a property ?
A property is an instance of a class deriving from the Property class. The property class offers methods to manage the content and the listeners of the instance.

A property may be modifiable or not. Why may a class built to fire actions on change be not modifiable ? Because properties may derive from other properties. As an example, it is possible to create an integer property that will be equal to the sum of two other integer property. This derived property should not be modifiable in itself.

Anyone may create a Property-derived class, but there are some useful classes already available.

### Base Property methods
To manage any property, the following methods are available.

#### Content
- `public boolean set(MyType object)` will set the value of the property if it is editable (MyType is the content type) and return true. Otherwise it will return false.
- `public MyType get()` will return the value of the property (MyType is the content type). The content may be null.

### Linking
A property may be linked to a source property. Any time the source property change, the linked one will change. A property may be linked to only one source, but a source may have several linked properties.
- `public boolean link(Property<?> source)` will link the property to the source if it is not already bound (see below) and return true. Otherwise it will return false.
- `public void unlink()` will remove the link to a source if any. This will not make the property editable it is was not.

### Binding
Linking is a source to destination process. Binding is a two-directional action. Binding two properties means that, any time one of them change, the other will also change. A restriction makes that a property may only be bound to one and only one other property.
- `public boolean bind(Property<?> duplicate)` will bind the property with the duplicate if it is not already bound and return true. Otherwise it will return false.
- `public void unbind()` will remove the bind to a duplicate if any.

### Listeners
The interest of properties is to be able to fire actions when the content change. A listener is a class implementing the functional interface ChangeListener and its method `public <T> void  changed(T oldValue, T newValue);`. Listeners may be added or removed to property. Just be careful : it is difficult to remove a lambda listener...
- `public void addListener(ChangeListener<?> listener)` will add the listener if it does not already have been added before.
- `public void removeListener(ChangeListener<?> listener)` will remove the listener if it has been added before.

## Object properties
It is possible to wrap a property around any object. Such a property will have the methods listed above and nothing else. To create such a property :
- `Property<MyType> myProperty = new Property<>()` will create the property and set its initial content to null.
- `Property<MyType> myProperty = new Property<>(MyType value)` will create the property and set its initial content to the given value.

To fire the fact that the object content has changed, the following methos has to be used:
- `public void fireChanged()` will trigger the changed process.

## Derived property types
Some types have been written for most-used cases.

### BooleanProperty
This is basically a Property<Boolean>, but with a few methods to enhance the possibilities :
- `public BooleanProperty not()` will create a derived BooleanProperty which value will always be the inverse of the value of the current boolean property.
- `public BooleanProperty and(BooleanProperty other)` will create a derived BooleanProperty which value will always be the `and` functions of the value of the current boolean property and the value of other one.
- `public BooleanProperty or(BooleanProperty other)` will create a derived BooleanProperty which value will always be the `or` functions of the value of the current boolean property and the value of other one.
- `public BooleanProperty nor(BooleanProperty other)` will create a derived BooleanProperty which value will always be the `nor` functions of the value of the current boolean property and the value of other one.
- `public BooleanProperty nand(BooleanProperty other)` will create a derived BooleanProperty which value will always be the `nand` functions of the value of the current boolean property and the value of other one.
- `public void removeListener(ChangeListener<?> listener)` will remove the listener if it has been added before.
- `public static BooleanProperty equals(BooleanProperty one, BooleanProperty other)` will create a derived BooleanProperty which value will always be the equality test result of values of the two given properties.

### DoubleProperty
This is basically a Property<Double>, but with a few methods to enhance the possibilities :
- `public DoubleProperty add(DoubleProperty other)` will create a derived DoubleProperty which value will always be the sum of the value of the current property and the value of other one.
- `public DoubleProperty minus(DoubleProperty other)` will create a derived DoubleProperty which value will always be the value of the current property minus the value of other one.
- `public DoubleProperty multiply(DoubleProperty other)` will create a derived DoubleProperty which value will always be the value of the current property multiplied by the value of other one.
- `public DoubleProperty divide(DoubleProperty other)` will create a derived DoubleProperty which value will always be the value of the current property divided by the value of other one.
- `public static DoubleProperty max(DoubleProperty one, DoubleProperty two)` will create a derived DoubleProperty which value will always be the maximum value of the two given properties.
- `public static DoubleProperty min(DoubleProperty one, DoubleProperty two)` will create a derived DoubleProperty which value will always be the minimum value of the two given properties.

### FloatProperty
This is basically a Property<Float>, but with a few methods to enhance the possibilities :
- `public FloatProperty add(FloatProperty other)` will create a derived FloatProperty which value will always be the sum of the value of the current property and the value of other one.
- `public DoubleProperty minus(FloatProperty other)` will create a derived FloatProperty which value will always be the value of the current property minus the value of other one.
- `public DoubleProperty multiply(FloatProperty other)` will create a derived FloatProperty which value will always be the value of the current property multiplied by the value of other one.
- `public DoubleProperty divide(FloatProperty other)` will create a derived FloatProperty which value will always be the value of the current property divided by the value of other one.
- `public static FloatProperty max(FloatProperty one, FloatProperty two)` will create a derived FloatProperty which value will always be the maximum value of the two given properties.
- `public static FloatProperty min(FloatProperty one, FloatProperty two)` will create a derived FloatProperty which value will always be the minimum value of the two given properties.

### IntegerProperty
This is basically a Property<Integer>, but with a few methods to enhance the possibilities :
- `public IntegerProperty add(IntegerProperty other)` will create a derived IntegerProperty which value will always be the sum of the value of the current property and the value of other one.
- `public IntegerProperty minus(IntegerProperty other)` will create a derived IntegerProperty which value will always be the value of the current property minus the value of other one.
- `public IntegerProperty multiply(IntegerProperty other)` will create a derived IntegerProperty which value will always be the value of the current property multiplied by the value of other one.
- `public IntegerProperty divide(IntegerProperty other)` will create a derived IntegerProperty which value will always be the value of the current property divided by the value of other one.
- `public static IntegerProperty maximum(IntegerProperty one, IntegerProperty two)` will create a derived IntegerProperty which value will always be the maximum value of the two given properties.
- `public static IntegerProperty minimim(IntegerProperty one, IntegerProperty two)` will create a derived IntegerProperty which value will always be the minimum value of the two given properties.

### LongProperty
This is basically a Property<Long>, but with a few methods to enhance the possibilities :
- `public LongProperty add(LongProperty other)` will create a derived LongProperty which value will always be the sum of the value of the current double property and the value of other one.
- `public LongProperty minus(LongProperty other)` will create a derived LongProperty which value will always be the value of the current  property minus the value of other one.
- `public LongProperty multiply(LongProperty other)` will create a derived LongProperty which value will always be the value of the current  property multiplied by the value of other one.
- `public LongProperty divide(LongProperty other)` will create a derived LongProperty which value will always be the value of the current  property divided by the value of other one.
- `public static LongProperty max(LongProperty one, LongProperty two)` will create a derived LongProperty which value will always be the maximum value of the two given properties.
- `public static LongProperty min(LongProperty one, LongProperty two)` will create a derived LongProperty which value will always be the minimum value of the two given properties.

### StringProperty
This is basically a Property<String>, but with a few methods to enhance the possibilities :
- `public StringProperty concat(StringProperty other)` will create a derived StringProperty which value will always be the value of the current property concatenated with the value of other one.
- `public StringProperty append(String suffix)` will create a derived StringProperty which value will always be the value of the current property suffixed with the given string.
- `public StringProperty prepend(String prefix)` will create a derived StringProperty which value will always be the value of the current property prefixed with the given string.
- `public StringProperty substring(int start, int length)` will create a derived StringProperty which value will always be the substring of the current property.
- `public StringProperty substring(IntegerProperty start, int length)` will create a derived StringProperty which value will always be the substring of the current property.
- `public StringProperty substring(int start, IntegerProperty length)` will create a derived StringProperty which value will always be the substring of the current property.
- `public StringProperty substring(IntegerProperty start, IntegerProperty length)` will create a derived StringProperty which value will always be the substring of the current property.

### ListProperty
This is a Property<List> which implements the List interface for a given list content type. It also has a specific method :
- `public IntegerProperty sizeProperty()` that will create a derived IntegerProperty which value will always be the length of the current property.

### MapProperty
This is a Property<Map> which implements the Map interface for a given list content type. It also has a specific method :
- `public IntegerProperty sizeProperty()` that will create a derived IntegerProperty which value will always be the length of the current property.

### TreeProperty
TBW.



