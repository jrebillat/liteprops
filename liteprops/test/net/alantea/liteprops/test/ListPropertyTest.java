package net.alantea.liteprops.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.liteprops.ListProperty;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class ListPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      ListProperty<String> prop = new ListProperty<>();
      
      String expected = "Hello World !";
      prop.add(expected);
      Assertions.assertEquals(1, prop.size());
      Assertions.assertEquals(expected, prop.get(0));
   }
   
   @Test
   public void T2_addAndRemoveTest()
   {
      ListProperty<String> prop = new ListProperty<>();
      
      String first = "Hello World !";
      prop.add(first);
      Assertions.assertEquals(1, prop.size());
      Assertions.assertEquals(first, prop.get(0));

      String second = "Hello you too...";
      prop.add(second);
      Assertions.assertEquals(2, prop.size());
      Assertions.assertEquals(second, prop.get(1));

      String third = "";
      prop.add(third);
      Assertions.assertEquals(3, prop.size());
      Assertions.assertEquals(third, prop.get(2));

      String fourth = null;
      prop.add(fourth);
      Assertions.assertEquals(4, prop.size());
      Assertions.assertEquals(fourth, prop.get(3));
      
      prop.remove(1);
      Assertions.assertEquals(3, prop.size());
      Assertions.assertEquals(first, prop.get(0));
      Assertions.assertEquals(third, prop.get(1));
      Assertions.assertEquals(fourth, prop.get(2));
      
      prop.clear();
      Assertions.assertEquals(0, prop.size());
      
      prop.add(first);
      Assertions.assertEquals(1, prop.size());
      Assertions.assertEquals(first, prop.get(0));
   }

   @Test
   public void T2_listenerTest()
   {
      String[] expected = new String[1];
      ListProperty<String> prop = new ListProperty<>();
      
      expected[0] = "Hello World !";
      prop.add(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(expected[0], prop.get(0)));
   }

   @Test
   public void T3_linkTest()
   {
      String expected = "";
      ListProperty<String> sourceProp = new ListProperty<>();
      ListProperty<String> linkedProp = new ListProperty<>();
      
      linkedProp.link(sourceProp);
      
      expected = "Hello World !";
      sourceProp.add(expected);
      Assertions.assertEquals(1, linkedProp.size());
      Assertions.assertEquals(expected, linkedProp.get(0));
      
      expected = "Hello you too...";
      sourceProp.add(expected);
      Assertions.assertEquals(2, linkedProp.size());
      Assertions.assertEquals(expected, linkedProp.get(1));

      ListProperty<String> anotherProp = new ListProperty<>();
      anotherProp.link(sourceProp);
      
      expected = "Bye.";
      sourceProp.add(expected);
      Assertions.assertEquals(3, linkedProp.size());
      Assertions.assertEquals(expected, linkedProp.get(2));
      Assertions.assertEquals(3, linkedProp.size());
      Assertions.assertEquals(expected, anotherProp.get(2));
   }

   @Test
   public void T4_unlinkTest()
   {
      ListProperty<String> sourceProp = new ListProperty<>();
      ListProperty<String> linkedProp = new ListProperty<>();
      
      linkedProp.link(sourceProp);

      sourceProp.add("Hello World !");
      Assertions.assertEquals(1, linkedProp.size());
      Assertions.assertEquals("Hello World !", linkedProp.get(0));

      linkedProp.unlink();

      sourceProp.add("Hello you too...");
      Assertions.assertEquals(2, sourceProp.size());
      Assertions.assertEquals(1, linkedProp.size());
   }

   @Test
   public void T5_bindTest()
   {
      String expected = "";
      ListProperty<String> firstProp = new ListProperty<>();
      ListProperty<String> secondProp = new ListProperty<>();
      
      secondProp.bind(firstProp);
      
      expected = "Hello World !";
      firstProp.add(expected);
      Assertions.assertEquals(1, secondProp.size());
      Assertions.assertEquals(expected, secondProp.get(0));
      
      expected = "Hello you too...";
      firstProp.add(expected);
      Assertions.assertEquals(2, secondProp.size());
      Assertions.assertEquals(expected, secondProp.get(1));
   }

   @Test
   public void T6_unbindTest()
   {
      ListProperty<String> firstProp = new ListProperty<>();
      ListProperty<String> secondProp = new ListProperty<>();
      
      secondProp.bind(firstProp);

      firstProp.add("Hello World !");
      Assertions.assertEquals(1, secondProp.size());
      Assertions.assertEquals("Hello World !", secondProp.get(0));

      secondProp.unbind();

      firstProp.add("Hello you too...");
      Assertions.assertEquals(2, firstProp.size());
      Assertions.assertEquals(1, secondProp.size());

      secondProp.remove(0);
      Assertions.assertEquals(2, firstProp.size());
      Assertions.assertEquals(0, secondProp.size());
   }
}
