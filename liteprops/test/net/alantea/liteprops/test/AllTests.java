package net.alantea.liteprops.test;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses({ 
   BooleanPropertyTest.class,
   IntegerPropertyTest.class,
   LongPropertyTest.class,
   FloatPropertyTest.class,
   DoublePropertyTest.class,
   StringPropertyTest.class,
   ListPropertyTest.class,
   MapPropertyTest.class
   })
public class AllTests
{

}
