package net.alantea.liteprops.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.liteprops.FloatProperty;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class FloatPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      FloatProperty prop = new FloatProperty();
      
      float expected = 1;
      prop.set(expected);
      Assertions.assertEquals(expected, (float)prop.get(), 1.0E-20);

      expected = -1;
      prop.set(expected);
      Assertions.assertEquals(expected, (float)prop.get(), 1.0E-20);

      expected = 0;
      prop.set(expected);
      Assertions.assertEquals(expected, (float)prop.get(), 1.0E-20);
      
      for (int i = 0; i < 100; i++)
      {
         expected = (float)(Math.random() * 200000.0 - 100000.0);
         prop.set(expected);
         Assertions.assertEquals(expected, (float)prop.get(), 1.0E-20);
      }
   }

   @Test
   public void T2_listenerTest()
   {
      float[] expected = new float[1];
      FloatProperty prop = new FloatProperty();
      
      expected[0] = 1.0f;
      prop.set(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(expected[0], (float)prop.get(), 1.0E-20));
      
      expected[0] = -1.0f;
      prop.set(expected[0]);

      expected[0] = 0.0f;
      prop.set(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(expected[0] + 1, (float)prop.get() + 1, 1.0E-20));
      
      expected[0] = 1.0f;
      prop.set(expected[0]);
   }

   @Test
   public void T3_linkTest()
   {
      float expected = 0.0f;
      FloatProperty sourceProp = new FloatProperty();
      FloatProperty linkedProp = new FloatProperty();
      
      linkedProp.link(sourceProp);
      
      expected = 1.0f;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (float)linkedProp.get(), 1.0E-20);
      
      expected = -1.0f;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (float)linkedProp.get(), 1.0E-20);

      FloatProperty anotherProp = new FloatProperty();
      anotherProp.link(sourceProp);
      
      expected = 1.0f;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (float)linkedProp.get(), 1.0E-20);
      Assertions.assertEquals(expected, (float)anotherProp.get(), 1.0E-20);
   }

   @Test
   public void T4_unlinkTest()
   {
      FloatProperty sourceProp = new FloatProperty();
      FloatProperty linkedProp = new FloatProperty();
      
      linkedProp.link(sourceProp);
      
      sourceProp.set(1.0f);
      Assertions.assertEquals(1.0f, (float)linkedProp.get(), 0);

      linkedProp.unlink();
      
      sourceProp.set(2.0f);
      Assertions.assertEquals(1.0f, (float)linkedProp.get(), 0);
   }

   @Test
   public void T5_bindTest()
   {
      float expected = 0;
      FloatProperty firstProp = new FloatProperty();
      FloatProperty secondProp = new FloatProperty();
      
      secondProp.bind(firstProp);
      
      expected = 1.0f;
      firstProp.set(expected);
      Assertions.assertEquals(expected, (float)secondProp.get(), 1.0E-20);
      
      expected = -1;
      secondProp.set(expected);
      Assertions.assertEquals(expected, (float)firstProp.get(), 1.0E-20);
   }

   @Test
   public void T6_unbindTest()
   {
      FloatProperty sourceProp = new FloatProperty();
      FloatProperty otherProp = new FloatProperty();
      
      otherProp.bind(sourceProp);
      
      sourceProp.set(1.0f);
      Assertions.assertEquals(1.0f, (float)otherProp.get(), 0);

      otherProp.unbind();
      
      sourceProp.set(2.0f);
      Assertions.assertEquals(1.0f, (float)otherProp.get(), 0);
      
      otherProp.set(3.0f);
      Assertions.assertEquals(2.0f, (float)sourceProp.get(), 0);
   }

   @Test
   public void T7_functionsTest()
   {
      float ref = 2.0f;
      FloatProperty refProp = new FloatProperty(ref);
      float expected = 10.0f;
      FloatProperty sourceProp = new FloatProperty(10);
      
      FloatProperty plusProp = sourceProp.add(refProp);
      FloatProperty minusProp = sourceProp.minus(refProp);
      FloatProperty multiplyProp = sourceProp.multiply(refProp);
      FloatProperty divideProp = sourceProp.divide(refProp);

      FloatProperty maxProp = FloatProperty.maximum(refProp, sourceProp);
      FloatProperty minProp = FloatProperty.minimum(refProp, sourceProp);
      
      Assertions.assertEquals(expected + ref, (float)plusProp.get(), 1.0E-20);
      Assertions.assertEquals(expected - ref, (float)minusProp.get(), 1.0E-20);
      Assertions.assertEquals(expected * ref, (float)multiplyProp.get(), 1.0E-20);
      Assertions.assertEquals(expected / ref, (float)divideProp.get(), 1.0E-20);
      Assertions.assertEquals(expected, (float)maxProp.get(), 1.0E-20);
      Assertions.assertEquals(ref, (float)minProp.get(), 1.0E-20);

      expected = -10.0f;
      sourceProp.set(expected);
      Assertions.assertEquals(expected + ref, (float)plusProp.get(), 1.0E-20);
      Assertions.assertEquals(expected - ref, (float)minusProp.get(), 1.0E-20);
      Assertions.assertEquals(expected * ref, (float)multiplyProp.get(), 1.0E-20);
      Assertions.assertEquals(expected / ref, (float)divideProp.get(), 1.0E-20);
      Assertions.assertEquals(ref, (float)maxProp.get(), 1.0E-20);
      Assertions.assertEquals(expected, (float)minProp.get(), 1.0E-20);
   }
}
