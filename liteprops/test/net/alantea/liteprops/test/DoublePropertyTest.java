package net.alantea.liteprops.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.liteprops.DoubleProperty;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class DoublePropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      DoubleProperty prop = new DoubleProperty();
      
      double expected = 1;
      prop.set(expected);
      Assertions.assertEquals(expected, (double)prop.get(), 1.0E-20);

      expected = -1;
      prop.set(expected);
      Assertions.assertEquals(expected, (double)prop.get(), 1.0E-20);

      expected = 0;
      prop.set(expected);
      Assertions.assertEquals(expected, (double)prop.get(), 1.0E-20);
      
      for (int i = 0; i < 100; i++)
      {
         expected = (double)(Math.random() * 200000.0 - 100000.0);
         prop.set(expected);
         Assertions.assertEquals(expected, (double)prop.get(), 1.0E-20);
      }
   }

   @Test
   public void T2_listenerTest()
   {
      double[] expected = new double[1];
      DoubleProperty prop = new DoubleProperty();
      
      expected[0] = 1.0;
      prop.set(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(expected[0], (double)prop.get(), 1.0E-20));
      
      expected[0] = -1.0;
      prop.set(expected[0]);

      expected[0] = 0.0;
      prop.set(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(expected[0] + 1, (double)prop.get() + 1, 1.0E-20));
      
      expected[0] = 1.0;
      prop.set(expected[0]);
   }

   @Test
   public void T3_linkTest()
   {
      double expected = 0.0;
      DoubleProperty sourceProp = new DoubleProperty();
      DoubleProperty linkedProp = new DoubleProperty();
      
      linkedProp.link(sourceProp);
      
      expected = 1.0;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (double)linkedProp.get(), 1.0E-20);
      
      expected = -1.0;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (double)linkedProp.get(), 1.0E-20);

      DoubleProperty anotherProp = new DoubleProperty();
      anotherProp.link(sourceProp);
      
      expected = 1.0;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (double)linkedProp.get(), 1.0E-20);
      Assertions.assertEquals(expected, (double)anotherProp.get(), 1.0E-20);
   }

   @Test
   public void T4_unlinkTest()
   {
      DoubleProperty sourceProp = new DoubleProperty();
      DoubleProperty linkedProp = new DoubleProperty();
      
      linkedProp.link(sourceProp);
      
      sourceProp.set(1.0);
      Assertions.assertEquals(1.0, (double)linkedProp.get(), 0);

      linkedProp.unlink();
      
      sourceProp.set(2.0);
      Assertions.assertEquals(1.0, (double)linkedProp.get(), 0);
   }

   @Test
   public void T5_bindTest()
   {
      double expected = 0;
      DoubleProperty firstProp = new DoubleProperty();
      DoubleProperty secondProp = new DoubleProperty();
      
      secondProp.bind(firstProp);
      
      expected = 1.0;
      firstProp.set(expected);
      Assertions.assertEquals(expected, (double)secondProp.get(), 1.0E-20);
      
      expected = -1;
      secondProp.set(expected);
      Assertions.assertEquals(expected, (double)firstProp.get(), 1.0E-20);
   }

   @Test
   public void T6_unbindTest()
   {
      DoubleProperty sourceProp = new DoubleProperty();
      DoubleProperty otherProp = new DoubleProperty();
      
      otherProp.bind(sourceProp);
      
      sourceProp.set(1.0);
      Assertions.assertEquals(1.0, (double)otherProp.get(), 0);

      otherProp.unbind();
      
      sourceProp.set(2.0);
      Assertions.assertEquals(1.0, (double)otherProp.get(), 0);
      
      otherProp.set(3.0);
      Assertions.assertEquals(2.0, (double)sourceProp.get(), 0);
   }

   @Test
   public void T7_functionsTest()
   {
      double ref = 2.0;
      DoubleProperty refProp = new DoubleProperty(ref);
      double expected = 10.0;
      DoubleProperty sourceProp = new DoubleProperty(10);
      
      DoubleProperty plusProp = sourceProp.add(refProp);
      DoubleProperty minusProp = sourceProp.minus(refProp);
      DoubleProperty multiplyProp = sourceProp.multiply(refProp);
      DoubleProperty divideProp = sourceProp.divide(refProp);

      DoubleProperty maxProp = DoubleProperty.maximum(refProp, sourceProp);
      DoubleProperty minProp = DoubleProperty.minimum(refProp, sourceProp);
      
      Assertions.assertEquals(expected + ref, (double)plusProp.get(), 1.0E-20);
      Assertions.assertEquals(expected - ref, (double)minusProp.get(), 1.0E-20);
      Assertions.assertEquals(expected * ref, (double)multiplyProp.get(), 1.0E-20);
      Assertions.assertEquals(expected / ref, (double)divideProp.get(), 1.0E-20);
      Assertions.assertEquals(expected, (double)maxProp.get(), 1.0E-20);
      Assertions.assertEquals(ref, (double)minProp.get(), 1.0E-20);

      expected = -10.0;
      sourceProp.set(expected);
      Assertions.assertEquals(expected + ref, (double)plusProp.get(), 1.0E-20);
      Assertions.assertEquals(expected - ref, (double)minusProp.get(), 1.0E-20);
      Assertions.assertEquals(expected * ref, (double)multiplyProp.get(), 1.0E-20);
      Assertions.assertEquals(expected / ref, (double)divideProp.get(), 1.0E-20);
      Assertions.assertEquals(ref, (double)maxProp.get(), 1.0E-20);
      Assertions.assertEquals(expected, (double)minProp.get(), 1.0E-20);
   }
}
