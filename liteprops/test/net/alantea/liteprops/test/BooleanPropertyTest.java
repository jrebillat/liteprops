package net.alantea.liteprops.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.liteprops.BooleanProperty;
import net.alantea.liteprops.IntegerProperty;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class BooleanPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      BooleanProperty prop = new BooleanProperty();
      
      boolean expected = true;
      prop.set(expected);
      Assertions.assertEquals(expected, (boolean)prop.get());

      expected = false;
      prop.set(expected);
      Assertions.assertEquals(expected, (boolean)prop.get());
   }

   @Test
   public void T2_listenerTest()
   {
      boolean[] expected = new boolean[1];
      BooleanProperty prop = new BooleanProperty();
      
      expected[0] = false;
      prop.set(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(expected[0], (boolean)prop.get()));
      
      expected[0] = true;
      prop.set(expected[0]);

      expected[0] = false;
      prop.set(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(!expected[0], !(boolean)prop.get()));
      
      expected[0] = true;
      prop.set(expected[0]);
   }

   @Test
   public void T3_linkTest()
   {
      boolean expected;
      BooleanProperty sourceProp = new BooleanProperty();
      BooleanProperty linkedProp = new BooleanProperty();
      
      linkedProp.link(sourceProp);
      
      expected = true;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (boolean)linkedProp.get());
      
      expected = false;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (boolean)linkedProp.get());

      BooleanProperty anotherProp = new BooleanProperty();
      anotherProp.link(sourceProp);
      
      expected = true;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (boolean)linkedProp.get());
      Assertions.assertEquals(expected, (boolean)anotherProp.get());
   }

   @Test
   public void T4_unlinkTest()
   {
      boolean expected;
      BooleanProperty sourceProp = new BooleanProperty();
      BooleanProperty linkedProp = new BooleanProperty();
      
      linkedProp.link(sourceProp);
      
      expected = true;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (boolean)linkedProp.get());
      
      expected = false;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (boolean)linkedProp.get());

      linkedProp.unlink();
      
      sourceProp.set(true);
      Assertions.assertEquals(false, (boolean)linkedProp.get());
      Assertions.assertEquals(true, (boolean)sourceProp.get());
   }

   @Test
   public void T5_bindTest()
   {
      BooleanProperty firstProp = new BooleanProperty();
      BooleanProperty secondProp = new BooleanProperty();
      
      secondProp.bind(firstProp);
      
      boolean expected = true;
      firstProp.set(expected);
      Assertions.assertEquals(expected, (boolean)secondProp.get());
      
      expected = false;
      secondProp.set(expected);
      Assertions.assertEquals(expected, (boolean)firstProp.get());
   }

   @Test
   public void T6_unbindTest()
   {
      BooleanProperty firstProp = new BooleanProperty();
      BooleanProperty secondProp = new BooleanProperty();
      
      secondProp.bind(firstProp);
      
      boolean expected = true;
      firstProp.set(expected);
      Assertions.assertEquals(expected, (boolean)firstProp.get());
      Assertions.assertEquals(expected, (boolean)secondProp.get());
      
      secondProp.unbind();

      firstProp.set(false);
      Assertions.assertEquals(false, (boolean)firstProp.get());
      Assertions.assertEquals(true, (boolean)secondProp.get());

      firstProp.set(true);
      secondProp.set(false);
      firstProp.set(expected);
      Assertions.assertEquals(true, (boolean)firstProp.get());
      Assertions.assertEquals(false, (boolean)secondProp.get());
   }

   @Test
   public void T7_functionsTest()
   {
      BooleanProperty refProp = new BooleanProperty();
      BooleanProperty sourceProp = new BooleanProperty();
      
      BooleanProperty notProp = sourceProp.not();
      BooleanProperty andProp = sourceProp.and(refProp);
      BooleanProperty nandProp = sourceProp.nand(refProp);
      BooleanProperty orProp = sourceProp.or(refProp);
      BooleanProperty norProp = sourceProp.nor(refProp);

      boolean expected = true;
      sourceProp.set(expected);
      Assertions.assertEquals(!expected, (boolean)notProp.get());

      boolean ref = true;
      refProp.set(ref);
      Assertions.assertEquals(expected && ref, (boolean)andProp.get());
      Assertions.assertEquals(!(expected && ref), (boolean)nandProp.get());
      Assertions.assertEquals(expected || ref, (boolean)orProp.get());
      Assertions.assertEquals(!(expected || ref), (boolean)norProp.get());

      ref = false;
      refProp.set(ref);
      Assertions.assertEquals(expected && ref, (boolean)andProp.get());
      Assertions.assertEquals(!(expected && ref), (boolean)nandProp.get());
      Assertions.assertEquals(expected || ref, (boolean)orProp.get());
      Assertions.assertEquals(!(expected || ref), (boolean)norProp.get());

      expected = false;
      sourceProp.set(expected);
      Assertions.assertEquals(!expected, (boolean)notProp.get());

      ref = true;
      refProp.set(ref);
      Assertions.assertEquals(expected && ref, (boolean)andProp.get());
      Assertions.assertEquals(!(expected && ref), (boolean)nandProp.get());
      Assertions.assertEquals(expected || ref, (boolean)orProp.get());
      Assertions.assertEquals(!(expected || ref), (boolean)norProp.get());

      ref = false;
      refProp.set(ref);
      Assertions.assertEquals(expected && ref, (boolean)andProp.get());
      Assertions.assertEquals(!(expected && ref), (boolean)nandProp.get());
      Assertions.assertEquals(expected || ref, (boolean)orProp.get());
      Assertions.assertEquals(!(expected || ref), (boolean)norProp.get());
   }

   @Test
   public void T8_equalsTest()
   {
      IntegerProperty firstProp = new IntegerProperty();
      IntegerProperty secondProp = new IntegerProperty();
      
      BooleanProperty equality = BooleanProperty.equals(firstProp, secondProp);
      
      firstProp.set(1);
      secondProp.set(1);
      Assertions.assertEquals(true, (boolean)equality.get());

      secondProp.set(2);
      Assertions.assertEquals(false, (boolean)equality.get());

      secondProp.set(1);
      Assertions.assertEquals(true, (boolean)equality.get());

   }
}
