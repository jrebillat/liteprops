package net.alantea.liteprops.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.liteprops.IntegerProperty;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class IntegerPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      IntegerProperty prop = new IntegerProperty();
      
      int expected = 1;
      prop.set(expected);
      Assertions.assertEquals(expected, (int)prop.get());

      expected = -1;
      prop.set(expected);
      Assertions.assertEquals(expected, (int)prop.get());

      expected = 0;
      prop.set(expected);
      Assertions.assertEquals(expected, (int)prop.get());
      
      for (int i = 0; i < 100; i++)
      {
         expected = (int)(Math.random() * 200000.0 - 100000.0);
         System.out.println(expected);
         prop.set(expected);
         Assertions.assertEquals(expected, (int)prop.get());
      }
   }

   @Test
   public void T2_listenerTest()
   {
      int[] expected = new int[1];
      IntegerProperty prop = new IntegerProperty();
      
      expected[0] = 1;
      prop.set(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(expected[0], (int)prop.get()));
      
      expected[0] = -1;
      prop.set(expected[0]);

      expected[0] = 0;
      prop.set(expected[0]);

      prop.addListener((o, v) -> Assertions.assertEquals(expected[0] + 1, (int)prop.get() + 1));
      
      expected[0] = 1;
      prop.set(expected[0]);
   }

   @Test
   public void T3_linkTest()
   {
      int expected = 0;
      IntegerProperty sourceProp = new IntegerProperty();
      IntegerProperty linkedProp = new IntegerProperty();
      
      linkedProp.link(sourceProp);
      
      expected = 1;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (int)linkedProp.get());
      
      expected = -1;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (int)linkedProp.get());

      IntegerProperty anotherProp = new IntegerProperty();
      anotherProp.link(sourceProp);
      
      expected = 1;
      sourceProp.set(expected);
      Assertions.assertEquals(expected, (int)linkedProp.get());
      Assertions.assertEquals(expected, (int)anotherProp.get());
   }

   @Test
   public void T4_unlinkTest()
   {
      IntegerProperty sourceProp = new IntegerProperty();
      IntegerProperty linkedProp = new IntegerProperty();
      
      linkedProp.link(sourceProp);
      
      sourceProp.set(1);
      Assertions.assertEquals(1, (int)linkedProp.get());

      linkedProp.unlink();
      
      sourceProp.set(2);
      Assertions.assertEquals(1, (int)linkedProp.get());
   }

   @Test
   public void T5_bindTest()
   {
      int expected = 0;
      IntegerProperty firstProp = new IntegerProperty();
      IntegerProperty secondProp = new IntegerProperty();
      
      secondProp.bind(firstProp);
      
      expected = 1;
      firstProp.set(expected);
      Assertions.assertEquals(expected, (int)secondProp.get());
      
      expected = -1;
      secondProp.set(expected);
      Assertions.assertEquals(expected, (int)firstProp.get());
   }

   @Test
   public void T6_unbindTest()
   {
      IntegerProperty sourceProp = new IntegerProperty();
      IntegerProperty otherProp = new IntegerProperty();
      
      otherProp.bind(sourceProp);
      
      sourceProp.set(1);
      Assertions.assertEquals(1, (int)otherProp.get());

      otherProp.unbind();
      
      sourceProp.set(2);
      Assertions.assertEquals(1, (int)otherProp.get());
      
      otherProp.set(3);
      Assertions.assertEquals(2, (int)sourceProp.get());
   }

   @Test
   public void T7_functionsTest()
   {
      int ref = 2;
      IntegerProperty refProp = new IntegerProperty(ref);
      int expected = 10;
      IntegerProperty sourceProp = new IntegerProperty(10);
      
      IntegerProperty plusProp = sourceProp.add(refProp);
      IntegerProperty minusProp = sourceProp.minus(refProp);
      IntegerProperty multiplyProp = sourceProp.multiply(refProp);
      IntegerProperty divideProp = sourceProp.divide(refProp);

      IntegerProperty maxProp = IntegerProperty.maximum(refProp, sourceProp);
      IntegerProperty minProp = IntegerProperty.minimum(refProp, sourceProp);
      
      Assertions.assertEquals(expected + ref, (int)plusProp.get());
      Assertions.assertEquals(expected - ref, (int)minusProp.get());
      Assertions.assertEquals(expected * ref, (int)multiplyProp.get());
      Assertions.assertEquals(expected / ref, (int)divideProp.get());
      Assertions.assertEquals(expected, (int)maxProp.get());
      Assertions.assertEquals(ref, (int)minProp.get());

      expected = -10;
      sourceProp.set(expected);
      Assertions.assertEquals(expected + ref, (int)plusProp.get());
      Assertions.assertEquals(expected - ref, (int)minusProp.get());
      Assertions.assertEquals(expected * ref, (int)multiplyProp.get());
      Assertions.assertEquals(expected / ref, (int)divideProp.get());
      Assertions.assertEquals(ref, (int)maxProp.get());
      Assertions.assertEquals(expected, (int)minProp.get());
   }
}
