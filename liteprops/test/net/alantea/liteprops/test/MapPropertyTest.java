package net.alantea.liteprops.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.liteprops.MapProperty;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class MapPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      MapProperty<String, String> prop = new MapProperty<>();

      String key = "Key1";
      String value = "Hello World !";
      prop.put(key, value);
      Assertions.assertEquals(1, prop.size());
      Assertions.assertEquals(value, prop.get(key));

      key = "Key2";
      value = "A value";
      prop.put(key, value);
      Assertions.assertEquals(2, prop.size());
      Assertions.assertEquals(value, prop.get(key));

      value = "Another value";
      prop.put(key, value);
      Assertions.assertEquals(2, prop.size());
      Assertions.assertEquals(value, prop.get(key));
   }

//   @Test
//   public void T2_listenerTest()
//   {
//      Map<> expected = new String[1];
//      MapProperty<String, String> prop = new MapProperty<>();
//      
//      expected[0] = "Hello World !";
//      prop.put(expected[0]);
//
//      prop.addListener((o, v) -> Assertions.assertEquals(expected[0], prop.get(0)));
//   }

   @Test
   public void T3_linkTest()
   {
      MapProperty<String, String> sourceProp = new MapProperty<>();
      MapProperty<String, String> linkedProp = new MapProperty<>();

      linkedProp.link(sourceProp);
      
      String key = "Key1";
      String value = "Hello World !";

      sourceProp.put(key, value);
      Assertions.assertEquals(1, linkedProp.size());
      Assertions.assertEquals(value, linkedProp.get(key));

      key = "Key2";
      value = "A value";
      sourceProp.put(key, value);
      Assertions.assertEquals(2, linkedProp.size());
      Assertions.assertEquals(value, linkedProp.get(key));

      MapProperty<String, String> anotherProp = new MapProperty<>();
      anotherProp.link(sourceProp);
      
      value = "Bye.";
      sourceProp.put(key, value);
      Assertions.assertEquals(2, linkedProp.size());
      Assertions.assertEquals(value, linkedProp.get(key));
      Assertions.assertEquals(2, anotherProp.size());
      Assertions.assertEquals(value, anotherProp.get(key));
   }

   @Test
   public void T4_unlinkTest()
   {
      MapProperty<String, String> sourceProp = new MapProperty<>();
      MapProperty<String, String> linkedProp = new MapProperty<>();

      sourceProp.put("Key1", "Hello World !");
      
      linkedProp.link(sourceProp);

      Assertions.assertEquals(1, linkedProp.size());
      Assertions.assertEquals("Hello World !", linkedProp.get("Key1"));

      linkedProp.unlink();

      sourceProp.put("Key1", "Hello you too...");
      Assertions.assertEquals("Hello World !", linkedProp.get("Key1"));
      Assertions.assertEquals("Hello you too...", sourceProp.get("Key1"));
   }

   @Test
   public void T5_bindTest()
   {
      MapProperty<String, String> firstProp = new MapProperty<>();
      MapProperty<String, String> secondProp = new MapProperty<>();
      
      secondProp.bind(firstProp);

      String key = "Key1";
      String value = "Hello World !";

      firstProp.put(key, value);
      Assertions.assertEquals(value, firstProp.get(key));
      Assertions.assertEquals(1, firstProp.size());
      Assertions.assertEquals(value, secondProp.get(key));
      Assertions.assertEquals(1, secondProp.size());
      
      value = "Hello you too...";
      secondProp.put(key, value);
      Assertions.assertEquals(1, secondProp.size());
      Assertions.assertEquals(value, secondProp.get(key));
      Assertions.assertEquals(1, firstProp.size());
      Assertions.assertEquals(value, firstProp.get(key));
   }

   @Test
   public void T6_unbindTest()
   {
      MapProperty<String, String> sourceProp = new MapProperty<>();
      MapProperty<String, String> otherProp = new MapProperty<>();

      sourceProp.put("Key1", "Hello World !");
      
      otherProp.bind(sourceProp);

      Assertions.assertEquals(1, otherProp.size());
      Assertions.assertEquals("Hello World !", otherProp.get("Key1"));

      otherProp.unbind();

      sourceProp.put("Key1", "Hello you too...");
      Assertions.assertEquals("Hello World !", otherProp.get("Key1"));
      Assertions.assertEquals("Hello you too...", sourceProp.get("Key1"));

      otherProp.put("Key1", "Bye !");
      Assertions.assertEquals("Hello you too...", sourceProp.get("Key1"));
      Assertions.assertEquals("Bye !", otherProp.get("Key1"));
   }
}
