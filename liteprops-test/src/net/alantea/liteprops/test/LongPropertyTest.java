package net.alantea.liteprops.test;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.liteprops.LongProperty;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LongPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      LongProperty prop = new LongProperty();
      
      long expected = 1;
      prop.set(expected);
      assertEquals(expected, (long)prop.get());

      expected = -1;
      prop.set(expected);
      assertEquals(expected, (long)prop.get());

      expected = 0;
      prop.set(expected);
      assertEquals(expected, (long)prop.get());
      
      for (int i = 0; i < 100; i++)
      {
         expected = (long)(Math.random() * 200000.0 - 100000.0);
         System.out.println(expected);
         prop.set(expected);
         assertEquals(expected, (long)prop.get());
      }
   }

   @Test
   public void T2_listenerTest()
   {
      long[] expected = new long[1];
      LongProperty prop = new LongProperty();
      
      expected[0] = 1;
      prop.set(expected[0]);

      prop.addListener((o, v) -> assertEquals(expected[0], (long)prop.get()));
      
      expected[0] = -1;
      prop.set(expected[0]);

      expected[0] = 0;
      prop.set(expected[0]);

      prop.addListener((o, v) -> assertEquals(expected[0] + 1, (long)prop.get() + 1));
      
      expected[0] = 1;
      prop.set(expected[0]);
   }

   @Test
   public void T3_linkTest()
   {
      long expected = 0;
      LongProperty sourceProp = new LongProperty();
      LongProperty linkedProp = new LongProperty();
      
      linkedProp.link(sourceProp);
      
      expected = 1;
      sourceProp.set(expected);
      assertEquals(expected, (long)linkedProp.get());
      
      expected = -1;
      sourceProp.set(expected);
      assertEquals(expected, (long)linkedProp.get());

      LongProperty anotherProp = new LongProperty();
      anotherProp.link(sourceProp);
      
      expected = 1;
      sourceProp.set(expected);
      assertEquals(expected, (long)linkedProp.get());
      assertEquals(expected, (long)anotherProp.get());
   }

   @Test
   public void T4_unlinkTest()
   {
      LongProperty sourceProp = new LongProperty();
      LongProperty linkedProp = new LongProperty();
      
      linkedProp.link(sourceProp);
      
      sourceProp.set(1L);
      assertEquals(1L, (long)linkedProp.get());

      linkedProp.unlink();
      
      sourceProp.set(2L);
      assertEquals(1L, (long)linkedProp.get());
   }

   @Test
   public void T5_bindTest()
   {
      long expected = 0;
      LongProperty firstProp = new LongProperty();
      LongProperty secondProp = new LongProperty();
      
      secondProp.bind(firstProp);
      
      expected = 1;
      firstProp.set(expected);
      assertEquals(expected, (long)secondProp.get());
      
      expected = -1;
      secondProp.set(expected);
      assertEquals(expected, (long)firstProp.get());
   }

   @Test
   public void T6_unbindTest()
   {
      LongProperty sourceProp = new LongProperty();
      LongProperty otherProp = new LongProperty();
      
      otherProp.bind(sourceProp);
      
      sourceProp.set(1L);
      assertEquals(1L, (long)otherProp.get());

      otherProp.unbind();
      
      sourceProp.set(2L);
      assertEquals(1L, (long)otherProp.get());
      
      otherProp.set(3L);
      assertEquals(2L, (long)sourceProp.get());
   }

   @Test
   public void T7_functionsTest()
   {
      long ref = 2;
      LongProperty refProp = new LongProperty(ref);
      long expected = 10;
      LongProperty sourceProp = new LongProperty(10);
      
      LongProperty plusProp = sourceProp.add(refProp);
      LongProperty minusProp = sourceProp.minus(refProp);
      LongProperty multiplyProp = sourceProp.multiply(refProp);
      LongProperty divideProp = sourceProp.divide(refProp);

      LongProperty maxProp = LongProperty.maximum(refProp, sourceProp);
      LongProperty minProp = LongProperty.minimum(refProp, sourceProp);
      
      assertEquals(expected + ref, (long)plusProp.get());
      assertEquals(expected - ref, (long)minusProp.get());
      assertEquals(expected * ref, (long)multiplyProp.get());
      assertEquals(expected / ref, (long)divideProp.get());
      assertEquals(expected, (long)maxProp.get());
      assertEquals(ref, (long)minProp.get());

      expected = -10;
      sourceProp.set(expected);
      assertEquals(expected + ref, (long)plusProp.get());
      assertEquals(expected - ref, (long)minusProp.get());
      assertEquals(expected * ref, (long)multiplyProp.get());
      assertEquals(expected / ref, (long)divideProp.get());
      assertEquals(ref, (long)maxProp.get());
      assertEquals(expected, (long)minProp.get());
   }
}
