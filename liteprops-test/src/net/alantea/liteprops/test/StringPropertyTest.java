package net.alantea.liteprops.test;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.liteprops.IntegerProperty;
import net.alantea.liteprops.StringProperty;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StringPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      StringProperty prop = new StringProperty();
      
      String expected = "Hello World !";
      prop.set(expected);
      assertEquals(expected, (String)prop.get());

      expected = "";
      prop.set(expected);
      assertEquals(expected, (String)prop.get());

      expected = null;
      prop.set(expected);
      assertEquals(expected, (String)prop.get());
   }

   @Test
   public void T2_listenerTest()
   {
      String[] expected = new String[1];
      StringProperty prop = new StringProperty();
      
      expected[0] = "Hello World !";
      prop.set(expected[0]);

      prop.addListener((o, v) -> assertEquals(expected[0], prop.get()));
      
      expected[0] = "Hello you too...";
      prop.set(expected[0]);

      expected[0] = "";
      prop.set(expected[0]);

      prop.addListener((o, v) -> assertEquals(expected[0] + "---", prop.get() + "---"));
      
      expected[0] = null;
      prop.set(expected[0]);
   }

   @Test
   public void T3_linkTest()
   {
      String expected = "";
      StringProperty sourceProp = new StringProperty();
      StringProperty linkedProp = new StringProperty();
      
      linkedProp.link(sourceProp);
      
      expected = "Hello World !";
      sourceProp.set(expected);
      assertEquals(expected, linkedProp.get());
      
      expected = "Hello you too...";
      sourceProp.set(expected);
      assertEquals(expected, linkedProp.get());

      StringProperty anotherProp = new StringProperty();
      anotherProp.link(sourceProp);
      
      expected = "Bye.";
      sourceProp.set(expected);
      assertEquals(expected, linkedProp.get());
      assertEquals(expected, anotherProp.get());
   }

   @Test
   public void T4_unlinkTest()
   {
      StringProperty sourceProp = new StringProperty();
      StringProperty linkedProp = new StringProperty();
      
      linkedProp.link(sourceProp);
      
      sourceProp.set("Hello World !");
      assertEquals("Hello World !", linkedProp.get());

      linkedProp.unlink();
      
      sourceProp.set("Hello you too...");
      assertEquals("Hello World !", linkedProp.get());
   }

   @Test
   public void T5_bindTest()
   {
      String expected = "";
      StringProperty firstProp = new StringProperty();
      StringProperty secondProp = new StringProperty();
      
      secondProp.bind(firstProp);
      
      expected = "Hello World !";
      firstProp.set(expected);
      assertEquals(expected, secondProp.get());
      
      expected = "Hello you too...";
      secondProp.set(expected);
      assertEquals(expected, firstProp.get());
   }

   @Test
   public void T6_unbindTest()
   {
      StringProperty sourceProp = new StringProperty();
      StringProperty otherProp = new StringProperty();
      
      otherProp.bind(sourceProp);
      
      sourceProp.set("Hello World !");
      assertEquals("Hello World !", otherProp.get());

      otherProp.unbind();
      
      sourceProp.set("Hello you too...");
      assertEquals("Hello World !", otherProp.get());
      
      otherProp.set("Bye !");
      assertEquals("Hello you too...", sourceProp.get());
   }

   @Test
   public void T7_functionsTest()
   {
      StringProperty refProp = new StringProperty();
      StringProperty addedProp = new StringProperty();
      
      StringProperty concatProp = refProp.concat(addedProp);
      StringProperty appendProp = refProp.append("OrMore");
      StringProperty prependProp = refProp.prepend("LessThan");

      String ref = "One";
      String added = "Two";
      
      refProp.set(ref);
      addedProp.set(added);
      assertEquals(ref + added, concatProp.get());
      assertEquals(ref + "OrMore", appendProp.get());
      assertEquals("LessThan" + ref, prependProp.get());

      ref = "Something";
      refProp.set(ref);
      assertEquals(ref + added, concatProp.get());
      assertEquals(ref + "OrMore", appendProp.get());
      assertEquals("LessThan" + ref, prependProp.get());
      
      added = "OrAnything";
      addedProp.set(added);
      assertEquals(ref + added, concatProp.get());
   }

   @Test
   public void T8_substringTest()
   {
      StringProperty refProp = new StringProperty("Test");
      IntegerProperty startProp = new IntegerProperty();
      IntegerProperty endProp = new IntegerProperty();
      
      
      StringProperty simpleProp = refProp.substring(0,1);
      StringProperty firstProp = refProp.substring(startProp, 4);
      StringProperty secondProp = refProp.substring(0, endProp);
      StringProperty bothProp = refProp.substring(startProp, endProp);

      String ref = "HelloOne";
      refProp.set(ref);
      int start = 0;
      int end = 1;
      
      refProp.set(ref);
      startProp.set(start);
      endProp.set(end);
      assertEquals(ref.substring(0,1), simpleProp.get());
      assertEquals(ref.substring(start,start + 4), firstProp.get());
      assertEquals(ref.substring(0,end), secondProp.get());
      assertEquals(ref.substring(start, start + end), bothProp.get());

      ref = "YouAreWelcome";
      refProp.set(ref);
      assertEquals(ref.substring(0,1), simpleProp.get());
      assertEquals(ref.substring(start,start + 4), firstProp.get());
      assertEquals(ref.substring(0,end), secondProp.get());
      assertEquals(ref.substring(start, start + end), bothProp.get());
      
      start = 3;
      end = 5;
      
      startProp.set(start);
      endProp.set(end);
      assertEquals(ref.substring(0,1), simpleProp.get());
      assertEquals(ref.substring(start,start + 4), firstProp.get());
      assertEquals(ref.substring(0,end), secondProp.get());
      assertEquals(ref.substring(start, start + end), bothProp.get());
   }
}
