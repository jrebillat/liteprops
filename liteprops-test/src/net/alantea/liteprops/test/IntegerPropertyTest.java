package net.alantea.liteprops.test;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.liteprops.IntegerProperty;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IntegerPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      IntegerProperty prop = new IntegerProperty();
      
      int expected = 1;
      prop.set(expected);
      assertEquals(expected, (int)prop.get());

      expected = -1;
      prop.set(expected);
      assertEquals(expected, (int)prop.get());

      expected = 0;
      prop.set(expected);
      assertEquals(expected, (int)prop.get());
      
      for (int i = 0; i < 100; i++)
      {
         expected = (int)(Math.random() * 200000.0 - 100000.0);
         System.out.println(expected);
         prop.set(expected);
         assertEquals(expected, (int)prop.get());
      }
   }

   @Test
   public void T2_listenerTest()
   {
      int[] expected = new int[1];
      IntegerProperty prop = new IntegerProperty();
      
      expected[0] = 1;
      prop.set(expected[0]);

      prop.addListener((o, v) -> assertEquals(expected[0], (int)prop.get()));
      
      expected[0] = -1;
      prop.set(expected[0]);

      expected[0] = 0;
      prop.set(expected[0]);

      prop.addListener((o, v) -> assertEquals(expected[0] + 1, (int)prop.get() + 1));
      
      expected[0] = 1;
      prop.set(expected[0]);
   }

   @Test
   public void T3_linkTest()
   {
      int expected = 0;
      IntegerProperty sourceProp = new IntegerProperty();
      IntegerProperty linkedProp = new IntegerProperty();
      
      linkedProp.link(sourceProp);
      
      expected = 1;
      sourceProp.set(expected);
      assertEquals(expected, (int)linkedProp.get());
      
      expected = -1;
      sourceProp.set(expected);
      assertEquals(expected, (int)linkedProp.get());

      IntegerProperty anotherProp = new IntegerProperty();
      anotherProp.link(sourceProp);
      
      expected = 1;
      sourceProp.set(expected);
      assertEquals(expected, (int)linkedProp.get());
      assertEquals(expected, (int)anotherProp.get());
   }

   @Test
   public void T4_unlinkTest()
   {
      IntegerProperty sourceProp = new IntegerProperty();
      IntegerProperty linkedProp = new IntegerProperty();
      
      linkedProp.link(sourceProp);
      
      sourceProp.set(1);
      assertEquals(1, (int)linkedProp.get());

      linkedProp.unlink();
      
      sourceProp.set(2);
      assertEquals(1, (int)linkedProp.get());
   }

   @Test
   public void T5_bindTest()
   {
      int expected = 0;
      IntegerProperty firstProp = new IntegerProperty();
      IntegerProperty secondProp = new IntegerProperty();
      
      secondProp.bind(firstProp);
      
      expected = 1;
      firstProp.set(expected);
      assertEquals(expected, (int)secondProp.get());
      
      expected = -1;
      secondProp.set(expected);
      assertEquals(expected, (int)firstProp.get());
   }

   @Test
   public void T6_unbindTest()
   {
      IntegerProperty sourceProp = new IntegerProperty();
      IntegerProperty otherProp = new IntegerProperty();
      
      otherProp.bind(sourceProp);
      
      sourceProp.set(1);
      assertEquals(1, (int)otherProp.get());

      otherProp.unbind();
      
      sourceProp.set(2);
      assertEquals(1, (int)otherProp.get());
      
      otherProp.set(3);
      assertEquals(2, (int)sourceProp.get());
   }

   @Test
   public void T7_functionsTest()
   {
      int ref = 2;
      IntegerProperty refProp = new IntegerProperty(ref);
      int expected = 10;
      IntegerProperty sourceProp = new IntegerProperty(10);
      
      IntegerProperty plusProp = sourceProp.add(refProp);
      IntegerProperty minusProp = sourceProp.minus(refProp);
      IntegerProperty multiplyProp = sourceProp.multiply(refProp);
      IntegerProperty divideProp = sourceProp.divide(refProp);

      IntegerProperty maxProp = IntegerProperty.maximum(refProp, sourceProp);
      IntegerProperty minProp = IntegerProperty.minimum(refProp, sourceProp);
      
      assertEquals(expected + ref, (int)plusProp.get());
      assertEquals(expected - ref, (int)minusProp.get());
      assertEquals(expected * ref, (int)multiplyProp.get());
      assertEquals(expected / ref, (int)divideProp.get());
      assertEquals(expected, (int)maxProp.get());
      assertEquals(ref, (int)minProp.get());

      expected = -10;
      sourceProp.set(expected);
      assertEquals(expected + ref, (int)plusProp.get());
      assertEquals(expected - ref, (int)minusProp.get());
      assertEquals(expected * ref, (int)multiplyProp.get());
      assertEquals(expected / ref, (int)divideProp.get());
      assertEquals(ref, (int)maxProp.get());
      assertEquals(expected, (int)minProp.get());
   }
}
