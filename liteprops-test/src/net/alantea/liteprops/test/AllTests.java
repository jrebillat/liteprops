package net.alantea.liteprops.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
   BooleanPropertyTest.class,
   IntegerPropertyTest.class,
   LongPropertyTest.class,
   FloatPropertyTest.class,
   DoublePropertyTest.class,
   StringPropertyTest.class,
   ListPropertyTest.class,
   MapPropertyTest.class
   })
public class AllTests
{

}
