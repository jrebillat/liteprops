package net.alantea.liteprops.test;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.liteprops.ListProperty;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ListPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      ListProperty<String> prop = new ListProperty<>();
      
      String expected = "Hello World !";
      prop.add(expected);
      assertEquals(1, prop.size());
      assertEquals(expected, prop.get(0));
   }
   
   @Test
   public void T2_addAndRemoveTest()
   {
      ListProperty<String> prop = new ListProperty<>();
      
      String first = "Hello World !";
      prop.add(first);
      assertEquals(1, prop.size());
      assertEquals(first, prop.get(0));

      String second = "Hello you too...";
      prop.add(second);
      assertEquals(2, prop.size());
      assertEquals(second, prop.get(1));

      String third = "";
      prop.add(third);
      assertEquals(3, prop.size());
      assertEquals(third, prop.get(2));

      String fourth = null;
      prop.add(fourth);
      assertEquals(4, prop.size());
      assertEquals(fourth, prop.get(3));
      
      prop.remove(1);
      assertEquals(3, prop.size());
      assertEquals(first, prop.get(0));
      assertEquals(third, prop.get(1));
      assertEquals(fourth, prop.get(2));
      
      prop.clear();
      assertEquals(0, prop.size());
      
      prop.add(first);
      assertEquals(1, prop.size());
      assertEquals(first, prop.get(0));
   }

   @Test
   public void T2_listenerTest()
   {
      String[] expected = new String[1];
      ListProperty<String> prop = new ListProperty<>();
      
      expected[0] = "Hello World !";
      prop.add(expected[0]);

      prop.addListener((o, v) -> assertEquals(expected[0], prop.get(0)));
   }

   @Test
   public void T3_linkTest()
   {
      String expected = "";
      ListProperty<String> sourceProp = new ListProperty<>();
      ListProperty<String> linkedProp = new ListProperty<>();
      
      linkedProp.link(sourceProp);
      
      expected = "Hello World !";
      sourceProp.add(expected);
      assertEquals(1, linkedProp.size());
      assertEquals(expected, linkedProp.get(0));
      
      expected = "Hello you too...";
      sourceProp.add(expected);
      assertEquals(2, linkedProp.size());
      assertEquals(expected, linkedProp.get(1));

      ListProperty<String> anotherProp = new ListProperty<>();
      anotherProp.link(sourceProp);
      
      expected = "Bye.";
      sourceProp.add(expected);
      assertEquals(3, linkedProp.size());
      assertEquals(expected, linkedProp.get(2));
      assertEquals(3, linkedProp.size());
      assertEquals(expected, anotherProp.get(2));
   }

   @Test
   public void T4_unlinkTest()
   {
      ListProperty<String> sourceProp = new ListProperty<>();
      ListProperty<String> linkedProp = new ListProperty<>();
      
      linkedProp.link(sourceProp);

      sourceProp.add("Hello World !");
      assertEquals(1, linkedProp.size());
      assertEquals("Hello World !", linkedProp.get(0));

      linkedProp.unlink();

      sourceProp.add("Hello you too...");
      assertEquals(2, sourceProp.size());
      assertEquals(1, linkedProp.size());
   }

   @Test
   public void T5_bindTest()
   {
      String expected = "";
      ListProperty<String> firstProp = new ListProperty<>();
      ListProperty<String> secondProp = new ListProperty<>();
      
      secondProp.bind(firstProp);
      
      expected = "Hello World !";
      firstProp.add(expected);
      assertEquals(1, secondProp.size());
      assertEquals(expected, secondProp.get(0));
      
      expected = "Hello you too...";
      firstProp.add(expected);
      assertEquals(2, secondProp.size());
      assertEquals(expected, secondProp.get(1));
   }

   @Test
   public void T6_unbindTest()
   {
      ListProperty<String> firstProp = new ListProperty<>();
      ListProperty<String> secondProp = new ListProperty<>();
      
      secondProp.bind(firstProp);

      firstProp.add("Hello World !");
      assertEquals(1, secondProp.size());
      assertEquals("Hello World !", secondProp.get(0));

      secondProp.unbind();

      firstProp.add("Hello you too...");
      assertEquals(2, firstProp.size());
      assertEquals(1, secondProp.size());

      secondProp.remove(0);
      assertEquals(2, firstProp.size());
      assertEquals(0, secondProp.size());
   }
}
