package net.alantea.liteprops.test;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.liteprops.MapProperty;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MapPropertyTest
{

   @Test
   public void T1_creationAndReadTest()
   {
      MapProperty<String, String> prop = new MapProperty<>();

      String key = "Key1";
      String value = "Hello World !";
      prop.put(key, value);
      assertEquals(1, prop.size());
      assertEquals(value, prop.get(key));

      key = "Key2";
      value = "A value";
      prop.put(key, value);
      assertEquals(2, prop.size());
      assertEquals(value, prop.get(key));

      value = "Another value";
      prop.put(key, value);
      assertEquals(2, prop.size());
      assertEquals(value, prop.get(key));
   }

//   @Test
//   public void T2_listenerTest()
//   {
//      Map<> expected = new String[1];
//      MapProperty<String, String> prop = new MapProperty<>();
//      
//      expected[0] = "Hello World !";
//      prop.put(expected[0]);
//
//      prop.addListener((o, v) -> assertEquals(expected[0], prop.get(0)));
//   }

   @Test
   public void T3_linkTest()
   {
      MapProperty<String, String> sourceProp = new MapProperty<>();
      MapProperty<String, String> linkedProp = new MapProperty<>();

      linkedProp.link(sourceProp);
      
      String key = "Key1";
      String value = "Hello World !";

      sourceProp.put(key, value);
      assertEquals(1, linkedProp.size());
      assertEquals(value, linkedProp.get(key));

      key = "Key2";
      value = "A value";
      sourceProp.put(key, value);
      assertEquals(2, linkedProp.size());
      assertEquals(value, linkedProp.get(key));

      MapProperty<String, String> anotherProp = new MapProperty<>();
      anotherProp.link(sourceProp);
      
      value = "Bye.";
      sourceProp.put(key, value);
      assertEquals(2, linkedProp.size());
      assertEquals(value, linkedProp.get(key));
      assertEquals(2, anotherProp.size());
      assertEquals(value, anotherProp.get(key));
   }

   @Test
   public void T4_unlinkTest()
   {
      MapProperty<String, String> sourceProp = new MapProperty<>();
      MapProperty<String, String> linkedProp = new MapProperty<>();

      sourceProp.put("Key1", "Hello World !");
      
      linkedProp.link(sourceProp);

      assertEquals(1, linkedProp.size());
      assertEquals("Hello World !", linkedProp.get("Key1"));

      linkedProp.unlink();

      sourceProp.put("Key1", "Hello you too...");
      assertEquals("Hello World !", linkedProp.get("Key1"));
      assertEquals("Hello you too...", sourceProp.get("Key1"));
   }

   @Test
   public void T5_bindTest()
   {
      MapProperty<String, String> firstProp = new MapProperty<>();
      MapProperty<String, String> secondProp = new MapProperty<>();
      
      secondProp.bind(firstProp);

      String key = "Key1";
      String value = "Hello World !";

      firstProp.put(key, value);
      assertEquals(value, firstProp.get(key));
      assertEquals(1, firstProp.size());
      assertEquals(value, secondProp.get(key));
      assertEquals(1, secondProp.size());
      
      value = "Hello you too...";
      secondProp.put(key, value);
      assertEquals(1, secondProp.size());
      assertEquals(value, secondProp.get(key));
      assertEquals(1, firstProp.size());
      assertEquals(value, firstProp.get(key));
   }

   @Test
   public void T6_unbindTest()
   {
      MapProperty<String, String> sourceProp = new MapProperty<>();
      MapProperty<String, String> otherProp = new MapProperty<>();

      sourceProp.put("Key1", "Hello World !");
      
      otherProp.bind(sourceProp);

      assertEquals(1, otherProp.size());
      assertEquals("Hello World !", otherProp.get("Key1"));

      otherProp.unbind();

      sourceProp.put("Key1", "Hello you too...");
      assertEquals("Hello World !", otherProp.get("Key1"));
      assertEquals("Hello you too...", sourceProp.get("Key1"));

      otherProp.put("Key1", "Bye !");
      assertEquals("Hello you too...", sourceProp.get("Key1"));
      assertEquals("Bye !", otherProp.get("Key1"));
   }
}
